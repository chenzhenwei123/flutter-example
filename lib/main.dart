
import 'dart:ui';

import 'package:flutter/material.dart';
// import './pages/index.dart';
// import './pages/bottom_bar_shape.dart';
// import './pages/animateMy.dart';
// import './pages/frost_glass.dart';
// import './pages/keep_state_demo.dart';
// import './pages/bar_search.dart';
// import './pages/get_photo.dart';
// import './pages/expand_title_demo.dart';
// import './pages/besier_line_demo.dart';
// import './pages/open_app_animation.dart';
// import './pages/cupertino_back.dart';
// import './pages/draggable_demo.dart';
// import './pages/lange_sure/lang.dart';
import "./pages/flutter_gov_user/first_app.dart";
void main() => {
      // runApp(MaterialApp(//项目入口必须有MaterialApp函数
      //   title: '页面返回带回数据',
      //   // home: MyColumnContainer()
      //   home: MyFirstPage(
      //       products: List.generate(50,
      //           (index) => Product('商品${index + 1}', '商品描述:这是商品${index + 1}'))),
      // ))
      runApp(MyApp())
    };
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,//关闭右上角bug图标
      title:'学习基本',
      /* home:Scaffold(
        body:Center(
          child: Container(
            // child:ContainerEle(),
            // child:MyList(),
            // child:ListBuildFactory(),
            // child:GrideListText(),
            // child:MyRowDiaplay(),
            // child:MyColumnDisplay(),
            // child:MyStachDemo(),
            child:MyCard(),
            alignment: Alignment.center,
            width:500.0,
            height:500.0,
            decoration: BoxDecoration(
              border:Border.all(width:2.0,color:Colors.blue),
              color:Colors.transparent
            ),
            margin:const EdgeInsets.all(5.0),
            padding:const EdgeInsets.all(20.0),
            
          )
        )
      ) */
      // home:MyWidgetList()
      // home:BottomNavigationBarClass()
      // home:BottomBarShape(),
      // home:MyPage1(),
      // home:FrostGlass(),
      // home:KeepPageStateDemo(),
      // home:BarSearchDemo(),
      // home:GetPhotoDemo(),
      // home:ExpansionTitleDemo(),
      // home:OpenAppAnimation(),
      // home:CupertinoBackDemo(),
      // home:DraggableTragetWidget(),
      // home:BottomNavigatorPage(),
      // home:JumpRoutePageAnimation(),
      home:SliverEleDemos(),
      routes: {
        'routeDemo':(context)=> NamedRouteDemo(),
        'demoTarge':(context)=>NamedRouteDemoTarget()
      },
      onGenerateRoute: (RouteSettings settings){
        return MaterialPageRoute(
          builder: (context){
            print(settings);
            print('onGenerateRoute1:${settings.name}-------argu:${settings.arguments}');
            return HookRoutedDemo();
          }
        );
      },
      
      // theme: ThemeData(
      //   primaryColor: Colors.lightBlue
      // ),
      // home:FirstScreen1(
      //   projectList:List.generate(30, (index) => ProjectObj('编号:${index+1}','数据值:666-${index+1}'))
      // )
    );
  }
}
//容器组件
class ContainerEle extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Container(
      child:  Container(
                child: Text(
                  '加油加油',
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style:TextStyle(
                    decoration: TextDecoration.underline,
                    decorationStyle: TextDecorationStyle.solid,
                    fontSize: 20.0,
                    color:Colors.white,
                  )
                ),
                width:100.0,
                height: 100.0,
                color:Colors.red
            ),
    );
  }
}

//列表
class MyList extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          // ListTile(
          //    leading: Icon(Icons.time_to_leave),
          //    title:Text('测试1')
          // ),
          Container(
            width:180.0,
            color: Colors.lightBlue,
            child: Text('测试1'),
          ),
          Container(
            width:180.0,
            color: Colors.lightBlue,
            child: Text('测试1'),
          ),
        ],
    );
  }
}
//列表生成器
List itemsList = List.generate(50,(index)=>'编号:${index + 1}');
class ListBuildFactory extends StatelessWidget {
 @override
 Widget build(BuildContext context){
   return Container(
    child: ListView.builder(
      itemCount:itemsList.length,
      itemBuilder:(context,index){
        return ListTile(
          leading: Icon(Icons.access_alarm_sharp),
          title:Text('w我是${itemsList[index]}')
        );
      }
    ),
   );
 }
}

//网格

class GrideListText extends StatelessWidget {

  @override
  Widget build(BuildContext context){
    return GridView(
      gridDelegate:SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount:4,
        crossAxisSpacing:8.0,
        mainAxisSpacing:16.0,
        childAspectRatio:2.0
      ),
     children: [//此处不知如何便利生成widget集合
       Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('1安安'),
      ),
      Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('安安'),
      ),
      Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('安安'),
      ),
      Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('安安'),
      ),
      Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('安安'),
      ),
      Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('安安'),
      ),
      Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('安安'),
      ),
      Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('安安'),
      ),
      Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('安安'),
      ),
      Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('安安'),
      ),
      Container(
        color:Colors.red,
        width:50.0,
        height: 50.0,
        padding:const EdgeInsets.all(2.0),
        child: Text('安安'),
      ),
     ],
    );
  }
}

//横向灵活布局
class MyRowDiaplay extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Row(
      children: [
        RaisedButton(
          color: Colors.blue,
          child: Text('按钮1'),
          onPressed: (){
            print('测试1');
          },
        ),
        Expanded(
          child: Container(
            color:Colors.black,
            child:Text('测试二'),
            // alignment: Alignment.center,
          ),
        ),
        RaisedButton(
          color:Colors.yellow,
          child: Text('测试三'),
          onPressed: (){
            print('测试三');
          },
        )
      ],
    );
  }
}
//纵向灵活布局
class MyColumnDisplay extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Container(
      child: Column(
        // mainAxisAlignment:MainAxisAlignment.start,
        crossAxisAlignment:CrossAxisAlignment.start,
        children: [
          Text('第一行'),
        Expanded(
          child: Container(
            child: Center(
              child: Text('第二行啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊',
              style: TextStyle(
                color:Colors.green
              ),
            )
            ),
            color:Colors.black
          )
        ),
        Text('第三行')
        ],
      ),
    );
  }
}

//层叠布局
//https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.taopic.com%2Fuploads%2Fallimg%2F110603%2F52-11060319503629.jpg&refer=http%3A%2F%2Fimg.taopic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1650508358&t=a71dc16daebc2c8f42db5f0b08f2bf25
class MyStachDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Stack(
            alignment:const FractionalOffset(0.5, 0.5),
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage('https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.taopic.com%2Fuploads%2Fallimg%2F110603%2F52-11060319503629.jpg&refer=http%3A%2F%2Fimg.taopic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1650508358&t=a71dc16daebc2c8f42db5f0b08f2bf25'),
                radius: 80.0,
              ),
              Positioned(
                child: Container(
                  child: Text('哈哈哈哈'),
                  padding: const EdgeInsets.all(5.0),
                  decoration:BoxDecoration(
                    color:Colors.blue
                  )
                ),
                top:10.0,
                left:40.0
              ),
              Positioned(
                child: Container(
                  child: Text('嘻嘻嘻嘻嘻'),
                  padding: const EdgeInsets.all(5.0),
                  decoration:BoxDecoration(
                    color:Colors.redAccent
                  )
                ),
                top:40.0,
                left:40.0
              )
              
              
            ],
          );
  }
}

//卡片布局
class MyCard extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    List myList = List.generate(20, (index) => '编号:${index+1}');
    return Card(
      child: ListView.builder(
        itemCount: myList.length,
        itemBuilder: (context,index){
          return Column(
            children: [
              ListTile(
                title:new Text('序列${myList[index]}',style: TextStyle(fontWeight: FontWeight.w500),),
                subtitle: new Text('${myList[index]}胜宏宇:1513938888'),
                leading: new Icon(Icons.account_box,color: Colors.lightBlue,),
              ),
              Divider()
            ],
          );
        },
      ),
    );
  }
}

//页面跳转
class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(title:Text('首页'),leading: Icon(Icons.ac_unit)),
      body: Center(
        child: RaisedButton(
          child: Text('跳转到详情页'),
          color:Colors.red,
          onPressed: (){
            Navigator.push(context,MaterialPageRoute(
              builder: (context)=> SecondScreen()
            ));
          },
        ),
      ),
    );
  }
}
class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: Text('详情页')),
      body: Center(
        child: RaisedButton(
          child: Text('返回首页'),
          color: Colors.yellow,
          onPressed: (){
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}

//页面返回并携带参数
class ProjectObj {
  final String name;
  final String myval;
  ProjectObj(this.name,this.myval);
}
// List projectList = List.generate(30, (index) => ProjectObj('编号:${index+1}','数据值:666-${index+1}'));
class FirstScreen1 extends StatelessWidget {
  //list元素类型要记得声明要不然要报错
  final List<ProjectObj> projectList;
  FirstScreen1({Key key,@required this.projectList}):super(key:key);
  @override
  Widget build(BuildContext context){
    //这个位置并不能构造数据
    // List projectList = List.generate(30, (index) => ProjectObj('编号:${index+1}','数据值:666-${index+1}'));
    return Scaffold(
      appBar: AppBar(
        title: Text('首页'),
        leading: Icon(Icons.home_repair_service),
      ),
      body: ListView.builder(
        itemCount:projectList.length,
        itemBuilder:(context,index){
          return ListTile(
            leading: Icon(Icons.time_to_leave),
            subtitle: Text('${projectList[index].myval}'),
            title: Text('${projectList[index].name}'),
            onTap:(){
              _navigatorAsyncMethods(context,projectList[index]);
              print('${projectList[index].name}');
            }
          );
        }
      ),
    );
    
  }
  //异步获取返回信息并提示
  _navigatorAsyncMethods(BuildContext context,ProjectObj project) async{
    var result = await Navigator.push(//记得加await
      context,
      MaterialPageRoute(
        builder: (context)=>SecondScreen2(project)
      )
    );
    print('result:$result');
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text('$result'),
    ));
  }
}
//详情页
class SecondScreen2 extends StatelessWidget {
  final ProjectObj projectObj;
  SecondScreen2(this.projectObj){
    print('${projectObj.name}');//事实证明初始化的时候是可以在这里做操作的
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('详情页'),
        leading: Icon(Icons.details),
      ),
      body:Center(
        child:Column(
          children: <Widget>[
            RaisedButton(
              color: Colors.blueAccent,
              child: Text('${projectObj.name}:${projectObj.myval}'),
              onPressed: (){
                Navigator.pop(context,projectObj.myval);
              },
            ),
            Image.asset('image/back.png')
          ],
        )
      )
    );
  }
}

//尝试遍历Widget
class MyWidgetList extends StatefulWidget {
  _MyWidgetList createState()=>_MyWidgetList();
}
class _MyWidgetList extends State<MyWidgetList>{

  final List<Widget> mylist=List();
  @override
  void initState(){
    for(int myindex=0;myindex<8;myindex++){
      mylist.add(
        Container(
          width:50.0,
          height: 50.0,
          color:Colors.blueAccent,
          child: Center(
            child: Text('编号:$myindex',)
          ),
          // alignment: Alignment.center,
        )
      );
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: Text('首页'),),
      body:Container(
        margin: const EdgeInsets.all(5.0),
        child: GridView(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
            crossAxisSpacing: 5.0,
            mainAxisSpacing: 5.0
          ),
          children:mylist
        )
      )
    );
  }
}
