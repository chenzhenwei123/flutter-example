import 'package:flutter/material.dart';
class BezierToDemoTest extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        
        child: Container(
          color: Colors.green,
          child: ClipPath(
            // clipper: BezierToDemo(),
            clipper: BezierCustomWave(),
            child: Container(
              height: 300.0,
              // color: Colors.deepPurple,
              color: Colors.blue,
            ),
          ),
        ),
      ),
    );
  }
}
class BezierToDemo extends CustomClipper<Path>{
  @override
  Path getClip(Size size){
    Path path = Path();
    //贝塞尔曲线总共需要6个点,其中2个为贝塞尔曲线坐标,其他为图的顶点
    path.lineTo(0, 0);//起点
    path.lineTo(0, size.height -80);//左侧底部顶点
    var firstBesierPoint = Offset(size.width/2,size.height);//贝塞尔的底部顶点(贝塞尔弧线需要起点和终点)
    var endBesierPoint = Offset(size.width, size.height - 180);
    path.quadraticBezierTo(firstBesierPoint.dx,firstBesierPoint.dy,endBesierPoint.dx,endBesierPoint.dy);
    path.lineTo(size.width, size.height-180);//右侧顶点
    path.lineTo(size.width,0);//右上顶点
    return path;
  }
  @override
  bool shouldReclip(CustomClipper<Path> oldClipper){
    return false;
  }
}

//波浪形的
class BezierCustomWave extends CustomClipper<Path>{
  @override
  Path getClip(Size size){
    Path path = Path();
    path.lineTo(0, 0);
    path.lineTo(0, size.height -80);
    var no1firstPoint = Offset(size.width/4,size.height-40);
    var no1endPoint = Offset(size.width/2,size.height-60);
    path.quadraticBezierTo(no1firstPoint.dx,no1firstPoint.dy,no1endPoint.dx,no1endPoint.dy);
    var no2firstPoint = Offset(size.width/4*3,size.height-120);
    var no2endPoint = Offset(size.width,size.height-60);
    path.quadraticBezierTo(no2firstPoint.dx,no2firstPoint.dy,no2endPoint.dx,no2endPoint.dy);
    path.lineTo(size.width, size.height - 60);
    path.lineTo(size.width,0);
    return path;

  }
  @override
  bool shouldReclip(CustomClipper<Path> oldClipper){
    return false;
  }
}