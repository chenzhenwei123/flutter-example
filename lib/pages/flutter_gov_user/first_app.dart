//flutter实战第二版
//第一章:第一个flutter应用
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math; 
/* class MyFristAPP1 extends StatefulWidget {
  @override
  _MyFristAPP1 createState()=>_MyFristAPP1();
}
class _MyFristAPP1 extends State<MyFristAPP1>{
   int _controllNum=0;
   void _addNum(){
     setState(() {
       _controllNum++;
     });
   }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        title: Text('第一个应用'),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _addNum
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body:Center(
        child: Column(
          children: [
            Text('this num is:'),
            Text('$_controllNum',style:TextStyle(color: Colors.red))
          ],
        ),
      )
    );
  }
} */

//第二章节:Widget简介
  //2.1.1 flutter的四棵树 
    //(1)widget树生成element树
    //(2)element生成renderObject树
    //(3)renderObject树生成layer树
    //(4)widget树和element树是一一对应的,其他的不一定

    //2.1.2 statelessWidget
      //(1)staatelessWidget继承自Widgeet类
      //(2)重写了createElement方法
    //2.1.3 statefullWidget 
      //(1)statelessWidget继承自Widgeet类
      //(2)重写了createElement方法
      //(3)和statelessWidget 不同的是 返回的element不同,同时还提供了createState接口用于创建和statefulWidget相关状态
      //(4)例如某个statefulWidget 在Widget树中多个不同位置被使用时,本质是在各个位置独立生成StatefulElement对应的State实例

    //2.1.4 State
      //1.概念:一个statefulWidget对应一个state,state即为statefulWiddget要维持的状态

      //2.state特点:
        //(1)state中的状态信息可以在widget构建时同步获取
        //(2)state中的信息可以在Widget的生命周期函数用用setState修改,用setState方法调用会通知flutter框架然后再重新build Widget树

      //3.state属性
        //(1)widget 表示与该State关联的Widget
        //(2) context 表示statefulWidget对应的BuildContext,例如适用于查找Widget树上的ScaffoldState等 
            // 查找父级最近的Scaffold对应的ScaffoldState对象:
            //ScaffoldState _state = context.findAncestorStateOfType<ScaffoldState>()!;

      //4. state生命周期
        //(1) initState:某个widget第一次被载入Widget时会调用,对于每一个State对象这个钩子函数只调用一次
        //(2) didChangeDepentencies:Widget依赖改变时会触发,例如主题,语言等改变,第一次出入widget时也会触发
        //(3) build:widget构建子树时调用,该钩子调用得比较多,其中包括:
            //1.initState后  2.didChangeDepentencies后 3.didUpdateWidgeet后 4.widget被移除后又在其他地方移入时
        //(4) didUpageWidget:当widget被重构时,flutter框架会调用widget.canUpdate函数对比新旧widget的key和runtimeType是否一致,一致时回调用该钩子
        //(5) deactivate:当state对象被移除widget树时会触发
        //(6) dispose:当state对象被从树中永久移除时触发,通常在该钩子中释放资源

    //2.1.5 在widget中获取State对象
      //1.通过context获取
          // class MyFristAPP1 extends StatefulWidget{
          //   const MyFristAPP1({Key key}):super(key: key);
          //   @override
          //   _MyFristAPP1 createState()=>_MyFristAPP1();
          // }
          class MyFristAPP1 extends StatelessWidget {
            @override
            Widget build(BuildContext context){
              return Scaffold(
                appBar: AppBar(
                  title: Text('获取state'),
                ),
                body: Center(
                  child: Builder(//切记要使用context是必须要使用Builder组件,本身有context的组件除外
                    builder: (context){
                      return IconButton(
                            icon: Icon(Icons.search),
                            onPressed: (){
                              // ScaffoldState _state = context.findAncestorStateOfType<ScaffoldState>();
                              //flutter默认有个约定,如果StafulWidget的状态是向外暴露的会默认提供静态方法of()获取其对应State对象
                              // ScaffoldState _state = Scaffold.of(context);//切记要使用context是必须要使用Builder组件,本身有context的组件除外
                              // _state.openDrawer();
                              Scaffold.of(context).showSnackBar(
                                SnackBar(content: Text('测试'))
                              );
                            },
                          );
                    }
                  ),
                ),
                drawer: Drawer(),
              );
            }
          }

      //2.GlobalKey:即一个全局储存State对象的一个对象,有点类似于js中将一些方法挂载到window对象中
        //问题:字面意思是整个app共用一个GlobalKey,但实际好像是一个类对应一个Globalkey--这个问题先留着
        class GloDemoClass extends StatelessWidget {
          static GlobalKey<ScaffoldState> _globalKey = GlobalKey();
          @override
          Widget build(BuildContext context){
            return Scaffold(
              key: _globalKey,
              appBar: AppBar(
                title: Text('测试globalKey'),

              ),
              body: Center(
                child: ElevatedButton(
                  child: Text('跳转到其他页'),
                  onPressed: (){
                    _globalKey.currentState.showSnackBar(
                      SnackBar(content: Text('aaaa'))
                    );
                    print(context);
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context){
                        return GlobalDemo2();
                      })
                    );
                  },
                ),
              ),
            );
          }
        }
        
        class GlobalDemo2 extends StatelessWidget {
          @override
          Widget build(BuildContext context){

            return Scaffold(
              appBar: AppBar(
                title: Text('其他页'),
              ),
              body: Center(child: ElevatedButton(
                child: Text('查看globalKey'),
                onPressed: (){
                  // print('globalKey:$_globalKey')
                  Navigator.of(context).pop();
                },
              ),),
            );
          }
        }
    //2.1.6 使用RenderObject 自定义Widge
        //statelessWidgt 和 statefulWidet 其实是为了堆积木提供一个平台,而像Text,Center这些积木是通过RenderObject来实现de 
        
        class CustomWidget extends LeafRenderObjectWidget {//LeafRenderObjectWidget继承自RenderObjectWidget
          //重写createRenderObject方法
          @override
          RenderObject createRenderObject(BuildContext context){
            return CustomRenderObject();
          }
          //重写更新方法
          @override
          void updateRenderObject(BuildContext context,CustomRenderObject renderObject){

            super.updateRenderObject(context, renderObject);
          }
        }
        
        class CustomRenderObject extends RenderBox{//RenderBox继承自RenderObject

          //重写布局方法
          @override
          void performLayout(){

          }
          
          //绘制
          @override
          void paint(PaintingContext context,Offset offset){

          }
        }

  //2.1.7 flutter sdk内置组件
    //基础组件库-import 'package:flutter/widgets.dart'; 
    //Material组件库-import 'package:flutter/material.dart';
    //curpetino-import 'package:flutter/curpetino.dart';
    //material 和curpetino组件库都是基于基础组件库的,所以引入两种风格组件库是就不用引入基础组件库了


  //2.2状态管理

    //flutter中状态管理其实和react和vue中状态管理类似
    //flutter官方给了选择管理位置的一些准则
      //1.如果状态是用户数据,例如选中状态,滑块位置等,最好由父widget管理
      //2.如何状态是本身的颜色,动画等外观样式,最好由widget本身管理
      //3.如果状态是有不同widget共享的,那么最好由这些widget共同的父widget来管理

    //2.2.1 widget自身管理自己的状态
      class ControllSelfState extends StatefulWidget {
        ControllSelfState({Key key}):super(key: key);
        @override
        _ControllSelfState createState()=> _ControllSelfState();
      }
      class _ControllSelfState extends State<ControllSelfState>{
        bool _active=false;
        void _changeState(){
          setState(() {
            _active = !_active;
          });
        }
        @override
        Widget build(BuildContext context){
          return Scaffold(
            appBar: AppBar(
              title: Text('apppBar'),
            ),
            body: Center(
              child: Container(
                width:100.0,
                height: 100.0,
                padding: const EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                  color:_active?Colors.green:Colors.grey
                ),
                child: Center(
                  child: TextButton(
                    child: Text(_active?'green':'grey'),
                    onPressed: _changeState,
                  ),
                ),
              ),
            ),
          );
        }
      }
    //2.2.2 父widget管理子widget
      //简单来说就是将改变相应数据的父widget方法传给子widget
      class ControllParentState extends StatefulWidget {

        ControllParentState({Key key}):super(key: key);
        @override
        _ControllParentState createState()=>_ControllParentState();
      }
      class _ControllParentState extends State<ControllParentState> {
        bool _active=false;
        //修改_active方法
        void _changeActive(bool newBool){
          print('parent:$newBool');
          setState(() {
            _active = newBool;
          });
        }
        @override
        void initState(){
          print('parent:init');
          super.initState();
        }
        @override
        Widget build(BuildContext context){
          final width = MediaQuery.of(context).size.width;
          final height = MediaQuery.of(context).size.height;
          print('width:$width------height:$height');
          print('context:$context');
          return Container(
            child: ChildContainer(
              childActive:_active,
              onchange:_changeActive
            ),
          );
        }
      }

      //子widget不用管理状态
      class ChildContainer extends StatelessWidget{
        final bool childActive;
        final ValueChanged<bool> onchange;
        ChildContainer({Key key,this.childActive,@required this.onchange}):super(key: key);
        //写一个方法调用父widget传递下来的方法
        void _changeChildActive(){
          print('child');
          onchange(!childActive);
        }
        @override
        Widget build(BuildContext context){
          return Scaffold(
            appBar:AppBar(
              title: Text('父widget管理'),
            ),
            body: Center(
             child: GestureDetector(
                child: Container(
                  padding: EdgeInsets.all(20.0),
                  width: 200.0,
                  height: 200.0,
                  decoration: BoxDecoration(
                    color:childActive?Colors.green:Colors.grey
                  ),
                  child: Center(
                    child: childActive?Text(
                      'grey',
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ):Text(
                      'green',
                      style: TextStyle(
                        color: Colors.black
                      ),
                    ),
                  ),
                ),
                onTap: _changeChildActive,
             ),
            ),
          );
        }
      }

    //2.2.3混合--官方例子用混合例子的实现方式用自身管理也能实现
        class BorderActiveView extends StatefulWidget {

          @override
          _BorderActiveView createState()=> _BorderActiveView();
        }
        class _BorderActiveView extends State<BorderActiveView> {

          bool _active = false;
          bool _paddingFlag=false;
          //按下事件
          void _tapDown(TapDownDetails  deatils){//TapDownDetails 
            setState(() {
              _paddingFlag = true;
            });
          }
          void _tapUp(TapUpDetails details){
            setState(() {
              _paddingFlag = false;
            });
          }
          void _tapCancel(){
            setState(() {
              _paddingFlag=false;
            });
          }
          void _onTap(){
            setState(() {
              _active=!_active;
            });
          }
          @override
          Widget build(BuildContext context){
            return Scaffold(
              appBar: AppBar(
                title: Text('点击盒子边框交互'),
              ),
              body: Center(
                child: GestureDetector(
                  onTapDown: _tapDown,
                  onTapUp: _tapUp,
                  onTapCancel: _tapCancel,
                  onTap: _onTap,
                  child: Container(
                    child: _active?Text('green',style: TextStyle(color:Colors.white,fontSize: 30.0)):Text('grey',style:TextStyle(
                      color: Colors.black,
                      fontSize: 20.0,
                      backgroundColor: Colors.red
                    )),
                    width:200.0,
                    height: 200.0,
                    decoration: BoxDecoration(
                      color:_active?Colors.green:Colors.grey,
                      border: _paddingFlag? Border.all(
                        color: Colors.blue[400],
                        width: 30.0
                      ):null
                    ),
                  ),
                ),
              ),
            );
          }
        }

    //2.2.4 全局状态管理  例如:在设置页设置全局的语言
      //1.实现一个全局的事件总线
      //2.依托包管理例如Provider,Redux
      //具体的实现在后面再进行补充
  
  //2.3 路由管理----在移动端路由一般指页面(Page)

    //2.3.1 MaterialPageRoute
      //一.特点:
        //1.继承自PageRoute
        //2.MaterialPageRoute 是Material组件库提供的组件,从而实现各平台统一的路由切换风格

      //二.MaterialPageRoute 参数
        //1.WidgetBuilder builder:构建路由页面的内容,返回值是Widget
        //2.RouteSettings setting:包含路由的配置信息,例如:路由名称,是否初始化路由(首页)
        //3. bool maintainState :栈入是是否对之前的路由页面进行缓存,默认缓存
        //4.bool fullScreenDialog :新的路由页面是否是一个全屏的模态框
        //5.自定义路由动画可以继承PageRoute改写
    
    //2.3.2 Navigator ---路由管理
      //常用方法 Navigator.push(BuildContext context,Route route) Navigator.pop(BuildContext contex,var result)
      //静态方法 Navigator.of(context).push(Route route);

    //2.3.3 命名路由
      //命名路由过程:
      //1.到MaterialApp类中新增一个Map<String,WidgetBuilder> routes参数 例如:MaterialApp(routes:{'findPage':FindPage()})
      //2.使用Navigator.of(context).pushNamed(String name,paramName:argument)
      //3.接收参数 ModalRoute.of(context).settings.paramName

      class NamedRouteDemo extends StatelessWidget {

        @override
        Widget build(BuildContext context){
          return Scaffold(
            body: Center(
              child: ElevatedButton(
                child: Text('跳转'),
                onPressed: (){
                  // Navigator.of(context).pushNamed('demoTarge',arguments:'data');//参数必须用arguments作为key
                  //使用onGenerateRoute时,下面的路由名hookRouteDemo不用去routes中注册才能拦截到
                  Navigator.of(context).pushNamed('hookRoueDemo',arguments:'data1');//参数必须用arguments作为key
                  // debugDumpApp();
                  debugDumpRenderTree();
                },
              ),
            ),
          );
        }
      }
      class NamedRouteDemoTarget extends StatelessWidget {

        @override
        Widget build(BuildContext context){
          return Scaffold(
            appBar: AppBar(
              title: Text('详情页'),
            ),
            body: Center(
              child:ElevatedButton(
                child: Text('返回'),
                onPressed: (){
                  print(ModalRoute.of(context).settings.arguments);
                  Navigator.of(context).pushNamed('routeDemo');
                },
              ),
            ),
          );
        }
      }

    //2.3.4 路由生成钩子

      //1.onGenerateRoute: 使用前提(1)使用的是命名路由 (2)在MaterialApp的routes路由列表中没有注册
        //Route<dynamic> Function(RouteSettings settings)
        //作用:一般用于验证要有无权限跳转到目标页:例如跳转详情页要验证登录等

        class HookRoutedDemo extends StatelessWidget {
          @override
          Widget build(BuildContext context){
            return Scaffold(
              appBar: AppBar(title: Text('路由构造函数')),
              body: Center(
                child: ElevatedButton(
                  child: Text('返回'),
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                ),
              ),
            );
          }
        }
      
      //2.NavigatorObservers :可以监听所有路由跳转动作 ---具体如何使用后面再看api

      //3.UnknownRoute:当打开一个不存在的命名路由是会进入 ---具体如何使用后面再看api




  //2.4 包管理 ---这一张后续回去再看

  //2.5 资源管理:安装包会包含代码和静态资源(图片,图标,json)2部分

    //2.5.1 assets
      //在pubspec.yaml文件中flutter处
      //例如:
      //flutter:
        //asset:
          // - imgae/back.png //image是文件名称

    //2.5.2 assets变体(variant)

      //在选择匹配当前设备分辨率是会使用到变体
      // …/graphics/my_icon.png
      // …/graphics/background.png
      // …/graphics/dark/background.png
      // …etc.

      // pubspec.yaml文件中
      // flutter:
      //   assets:
      //     - graphics/background.png

      // 那么这两个graphics/background.png和graphics/dark/background.png 都将包含在您的 asset bundle中。
      // 前者被认为是_main asset_ （主资源），后者被认为是一种变体（variant）

    
    //2.5.3加载文本Asset
      //1.每一个flutter应用都有一个rootBundle对象:可以通过import 'package:flutter/server.dart' 引入全局静态rootBendle来加载asset资源
      //2.使用父级Widget运行时替换得到asseetBundl来加载,具体:DefaultAssetBundle.of(context).loadStr(url);

      //例如:
        // import 'dart:async' show Future;
        // import 'package:flutter/services.dart' show rootBundle;

        // Future<String> loadAsset() async {
        //   return await rootBundle.loadString('assets/config.json');
        // }

    //2.5.4 加载图片   --包管理和资源管理先看资源管理再看包管理

  //2.6 调试flutter 应用---这一杰不好理解先跳过

    //2.6.1 日志与断点

      //1.debugger()函数声明
        //使用前提:1.用Dart Observatory（或另一个Dart调试器，例如IntelliJ IDE中的调试器
                  //2.添加import 'dart:developer';
        
        //使用示例:debugger()语句采用一个可选when参数，我们可以指定该参数仅在特定条件为真时中断，如下所示：
                  //void someFunction(double offset) {
                  //   debugger(when: offset > 30.0);
                  //   // ...
                  // }

      //2.print和debuggerPrint(防止print打印丢失一些数据行可以使用这个)

//第三章 基础组件

    //3.1文本样式

      //3.1.1 文本及样式 Text

        class TextaDemo extends StatelessWidget {

          @override 
          Widget build(BuildContext context){
            return Scaffold(
              appBar: AppBar(
                title: Text('文本'),
              ),
              body: Center(
                child: Container(
                  width: 300.0,
                  height: 600.0,
                  child: Text(
                    '文本内容-文本内容-文本内容-文本内容-文本内容-文本内容-文本内容-文本内容-文本内容-文本内容-文本内容-文本内容-',
                    maxLines:10,
                    overflow: TextOverflow.ellipsis,
                    textScaleFactor: 1,//一般用于系统字体设置
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18.0, //一般用于文本的的字体大小控制,而textScaleFactor一般用于系统字体
                      background: Paint()..color=Colors.yellow,
                      fontFamily: "Courier",
                      height: 1.8  //行高是一个因子 具体等于fontSize*height
                    ),
                  ),
                ),
              ),
            );
          }
        }

      //3.1.2 TextSpan :代表一个文本片段
          //定义:
          // const TextSpan({
          //   TextStyle style,
          //   String text,
          //   List<TextSpan> children,
          //   GestureRecognizer recognizer
          // })

          class TextDemo1 extends StatelessWidget {

            @override
            Widget build(BuildContext buildContext){
              return Scaffold(
                appBar: AppBar(
                  title: Text('富文本测试'),
                ),
                body: Center(
                  child: Container(
                    child: Text.rich(TextSpan(
                      text: '',
                      // recognizer: ,后面再看
                      children: [
                        TextSpan(
                          text: 'http:',
                          style: TextStyle(
                            color: Colors.green,
                            background: Paint()..color=Colors.pink,
                            fontSize: 20.0
                          )
                        ),
                        TextSpan(
                          text: 'www.baidu.com',
                          style: TextStyle(
                            color: Colors.red,
                            background: Paint()..color=Colors.blue
                          )
                        )
                      ]
                    )),
                  ),
                ),
              );
            }
          }

      //3.1.3 DefaultTextStyle:文本默认样式是可以继承的,如果不继承可以在子Text的style中加inherit:false
          
          class InheritTextStyleDemo extends StatelessWidget {

            @override
            Widget build(BuildContext buildContext){
              return Scaffold(
                appBar: AppBar(
                  title: Text('文本样式继承'),
                ),
                body: Center(
                  child: Container(
                    child: DefaultTextStyle(
                      style: TextStyle(
                        fontSize: 20.0,
                        background: Paint()..color=Colors.red
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('新闻'),
                          Text('政治'),
                          Text(
                            '历史',
                            style: TextStyle(
                              background: Paint()..color=Colors.blue,
                              inherit: false
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }
          }

      //3.1.4 字体引入使用
        //3.1.4.1 asset中字体使用步骤:
          //1.在pubspec.yaml中声明他们,和声明image类似,例如:
            
            // flutter:
            //   fonts:
            //     - family: Raleway
            //       fonts:
            //         - asset: assets/fonts/Raleway-Regular.ttf
            //         - asset: assets/fonts/Raleway-Medium.ttf
            //           weight: 500
            //         - asset: assets/fonts/Raleway-SemiBold.ttf
            //           weight: 600
            //     - family: AbrilFatface
            //       fonts:
            //         - asset: assets/fonts/abrilfatface/AbrilFatface-Regular.ttf
          
          //2.在需要的地方使用字体
            // 声明文本样式
              // const textStyle = const TextStyle(
              //   fontFamily: 'Raleway',
              // );

              // // 使用文本样式
              // var buttonText = const Text(
              //   "Use the font for this text",
              //   style: textStyle,
              // );

          //
        
        //3.1.4.2 package中字体使用 -有2种方式,目前只弄清楚第一种

          //1.使用包名:假设字体在my_package包中
            // const textStyle = const TextStyle(
            //   fontFamily: 'Raleway',
            //   package: 'my_package', //指定包名 必须值
            // );
  
  //3.2 按钮

    //由来:无论ElevatedButton,TextButton,IconButton都是继承于RowMaterialButton
    //共同特点:1.点击时都有外扩动画 2.都提供了onPress函数,如果不给默认按钮禁用

    //分类:
        //1.ElevatedButton,都有背景颜色和阴影的按钮,按下阴影扩大
        //2.TextButton,背景透明不带阴影,按下有背景
        //3.outlineButton,背景透明无阴影,按下边框边亮
        //4.iconButton ,无背景无阴影,按下有背景
        //5.以上每个都有一个.icon()函数,其有icon,label,onPress三个参数

    class ButtonListDemo extends StatelessWidget {
      @override
      Widget build(BuildContext buildContext){
        return Scaffold(
          appBar: AppBar(
            title: Text('按钮列表'),
          ),
          body: Center(
            child: Column(
              children: [
                ElevatedButton(
                  child: Text('elevated'),
                  onPressed: (){

                  },
                ),
                TextButton(
                  child: Text('textButton'),
                  onPressed: (){

                  },
                ),
                OutlineButton(
                  child: Text('OutlineButton'),
                  onPressed: (){

                  },
                ),
                IconButton(
                  icon: Icon(Icons.radio_button_checked),
                  onPressed: (){

                  },
                ),
                OutlineButton.icon(
                  icon: Icon(Icons.home),
                  label: Text('iconlabel'),
                  onPressed: (){

                  },
                  autofocus: false,
                )
              ],
            ),
          ),
        );
      }
    }
  
  //3.3图片和icon
    //3.3.1 图片:分为加载静态图片和网络图片2中方式
      //1.静态图片加载步骤 :(1).在根目录新建image文件夹并放入back.png图片 
                          //(2).在pubspec.yaml文件中 fluter 处添加:
                                  //  assets:
                                  // - image/back.png  ( 由于 yaml 文件对缩进严格，所以必须严格按照每一层两个空格的方式进行缩进，此处 assets 前面应有两个空格)

                          //(3)组件加载,有2种方式: Image(image:AssetImage('image/back.png),width:100) 和Image.asset('image/back.png',width:100.0)

      //2.动态图片加载 2种方式 Image(image:NetworkImage('http:www.baidu.com/back.png),width:100.0)和Image.network('http:www.baidu.com',width:100.0)

      //Image组件参数
        // const Image({
        //   url,
        //   width:100.0,//图片宽度
        //   height:100.0,//图片高度
        //   color:Colors.blue,//图片混合色值,
        //   colorBlendMode:BlendMode.difference,//混合模式
        //   fit:BoxFit.contain,//缩放模式
        //   repeat:ImageRepeat.noRepeat,//重复方式
        //   alignment:  Alignment.center,//对齐方式

        // })

      //Image缓存:flutter对加载过的图片是有缓存的,这个后面进阶部分再说


    //3.3.2  ICON

      //1.flutter 官方图片可以直接通过Icon组件调用
      //2.使用自定义字体图标(外部下载的用ttf格式)
        //步骤:
                //1.在根目录下新建fonts文件夹,将下载的字体图标文件放入其中 例如目录为:fonts/iconfont.ttf
                //2.在pubspec.yaml文件中导入
                  //例如:
                  //  fonts:
                  //   - family:myIcon #指定一个字体名字
                  //     fonts:
                  //       - asset:fonts/iconfont.ttf

                //3.为了方便使用我们定义一个MyIcont类
                  class MyIcont {
                    //book图标
                    static const IconData book = const IconData(
                      0xe614,
                      fontFamily: 'myIcon',
                      matchTextDirection:true
                    );
                    // 微信图标
                    static const IconData wechat = const IconData(
                        0xec7d,  
                        fontFamily: 'myIcon', 
                        matchTextDirection: true
                    );
                  }

                //4.使用
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   children: <Widget>[
                  //     Icon(MyIcons.book,color: Colors.purple),
                  //     Icon(MyIcons.wechat,color: Colors.green),
                  //   ],
                  // )

  //3.4单选开关和复选框

    //Switch和Checkbox特别一般都是父亲组件控制状态,其中Checkbox有一个tristate,当其为true时,Checkbox的状态会由原来的false和true再增加一个null状态

    //Switch和Checkbox常用属性 value,activeColor,onchange属性,其中Checkbox大小是无法改变,Switch只能改变宽度

    class SelectDemeo extends StatefulWidget {
      @override
      _SelectDemeo createState()=>_SelectDemeo();

    }
    class _SelectDemeo extends State<SelectDemeo>{
      bool _singleFlag = false;
      bool _numFlags = false;

      @override
      Widget build(BuildContext buildContext){
        return Scaffold(
          appBar: AppBar(
            title: Text('选择框测试'),
          ),
          body: Center(
            child: Column(
              children: [
                Switch(
                  value: _singleFlag,
                  activeColor: Colors.blue,
                  onChanged: (value){
                    print(value);
                    setState(() {
                      _singleFlag=value;
                    });
                  },
                ),
                Checkbox(
                  value: _numFlags,
                  activeColor: Colors.green,
                  onChanged: (value){
                    print(value);
                    setState(() {
                      _numFlags = value;
                    });
                  },
                )
              ],
            ),
          ),
        );
      }
    }

  
  //3.5输入框及表单  Material组件库提供了TextField和表单组件From
      
    //3.5.1 TextField
      //关键属性
        // const TextField({
        //   TextEditingController controller,//控制器用于获取文本内容,监听文本内容变化,多偏向初始化相关功能
        //   FocusNode focusNode,//用于控制TextField是否占有键盘输入的焦点,键盘交互的句柄
        //   InputDecoration decoration,//TextField的外观显示,提示文本,背景颜色,边框等
        //   TextInputType keyboardType,//键盘类型 
        //                                 //text -文本键盘
        //                                 //multiline-多行文本 与maxLine搭配使用
        //                                 //number -数字键盘
        //                                 //phone-优化后的电话号码键盘,显示"* #'
        //                                 //datetime-日期键盘,android显示'-'
        //                                 //emailAddress-邮件键盘,会显示'@'
        //                                 //url-url垫盘 ,显示'/'
        //   TextInputAction textInputAction,//回车键图标
        //   TextStyle style,//正在编辑的文本样式
        //   TextAlign textAlign,//对齐方式
        //   bool autoFucos,//自动聚焦
        //   bool obscureText,//密码输入是否隐藏字段
        //   int maxLine,//最大行
        //   int maxLength,//最大字数,会显示还剩多少可输入
        //   this.maxLengthEnforcement,//maxLengthEnforcement决定当输入文本长度超过maxLength时如何处理，如截断、超出等
        //   ToolbarOptions?toolbarOptions,//长按或者鼠标右击出现的菜单包括 copy、cut、paste 以及 selectAll
        //   ValueChanged<String> onChanged,//内容改变回调,与controll监听类似
        //   VoidCallback onEditingComplete,//按回车触发回调
        //   ValueChanged<String> onSubmitted,//回车触发回调
        //   List<TextInputFormatter> inputFormatters,//校验规则
        //   bool enabled,//是否禁止输入
        //   this.cursorWidth = 2.0,
        //   this.cursorRadius,
        //   this.cursorColor,
        //   this.onTap,
        // });


      //获取文本信息和监听文本变化
        class InputTestDemo extends StatefulWidget {
          _InputTestDemo createState()=>_InputTestDemo();
        }
        class _InputTestDemo extends State<InputTestDemo> {
          //要点一:获取文本内容有2中方式:1.自身的onchange事件,onEditingComplete事件 2.通过controller获取
          //要点二:
          String mystr = '';
          TextEditingController _mycontroller = TextEditingController();
          @override
          void initState(){
            _mycontroller.text = '初始化用户名:陈铁柱';
            _mycontroller.addListener(() {
              setState(() {
                mystr=_mycontroller.text;
              });
              //  _mycontroller.selection = TextSelection(
              //   baseOffset: 2,//初始化进来默认选中后面的,编辑时后面的只能填一个44
              //   extentOffset: _mycontroller.text.length
              // );
            });
            
            super.initState();
          }
          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('textField'),
              ),
              body: Container(
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _mycontroller,
                    autofocus: true,
                    keyboardType: TextInputType.url,
                    maxLength:20,
                    // onEditingComplete:(){
                    //   print('aaaa');
                    // } ,
                    onSubmitted: (val){
                      print('val:$val');
                    },
                    cursorWidth:8.0,
                    // cursorRadius: Radius.circular(50.0),//光标圆角
                    //cursorColor:Colors.blue,//光标颜色
                    decoration: InputDecoration(
                      hintStyle: TextStyle(
                        color: Colors.red
                      ),
                      // labelStyle: TextStyle(color:Colors.yellow),
                      labelText: "用户名",
                      hintText: "用户名或邮箱",
                      prefixIcon: Icon(Icons.person),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.yellow,width: 2.0),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green,width: 2.0)
                      )
                    ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                      labelText: "密码",
                      hintText: "您的登录密码",
                      prefixIcon: Icon(Icons.lock)
                    ),
                    obscureText: true,
                  ),
                  Center(
                    child: Text(mystr,style: TextStyle(fontSize: 30,color: Colors.green,),
                  ))
                ],
              )
              )
            );
          }
        }

      //控制焦点: 要点--1.获取焦点范围要通过FocusScope.of(context)获取,2.聚焦focusScopeNode.requestFocus(focus1) 3.失焦后键盘自动关闭focus1.unfoces()


       class ControllFocusDemo extends StatefulWidget {
         @override
         _ControllFocusDemo createState()=>_ControllFocusDemo();
       }
       class _ControllFocusDemo extends State<ControllFocusDemo>{
         List<InputMap> inputData=List<InputMap>();
         int _index=0;
         FocusScopeNode focusScopeNode;
         @override
         void initState(){
           for(int index=0;index<6;index++){
             //监听焦点状态改变
             FocusNode focus = FocusNode();
             focus.addListener(() {
               //当聚焦和失焦时都会进入
               print('hasFocus-index$index:${focus.hasFocus}');
             });
             inputData.add(
               InputMap(focus:focus,name:'focus$index',focusFlag: index==0?true:false,index: index)
             );
           }
           super.initState();
         }
         @override
         Widget build(BuildContext buildContext){
           return Scaffold(
             appBar: AppBar(
               title: Text('控制焦点'),
             ),
             body: Container(
               child: Column(
                 children: [
                   Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: inputData.map((item){
                        return TextField(
                          autofocus: item.focusFlag,
                          focusNode: item.focus,
                          decoration: InputDecoration(
                            labelText: item.name
                          ),
                        );
                      }).toList()
                   ),
                  Builder(builder: (context){
                    return  ElevatedButton(
                        child: Text('下一个'),
                        onPressed: (){
                          
                          if(_index==(inputData.length-1)){
                            setState(() {
                              _index=0;
                            });
                            
                          }else{
                            setState(() {
                              _index++;
                            });
                          }
                          if(focusScopeNode==null){
                            focusScopeNode = FocusScope.of(context);
                          }
                          focusScopeNode.requestFocus(inputData[_index].focus);
                          // inputData[_index].focus.unfocus();
                          print(inputData[_index].name);
                         
                        },
                      );
                  })
                 ],
               ),
             )
           );
         }
       }
       class InputMap {
         int index;
         bool focusFlag;
         FocusNode focus;
         String name;
         InputMap({this.focus,this.name,this.focusFlag,this.index});
       }

    //3.5.2 Form表单

      //主要组成部分 Form类和继承了TextField类和FormField类的TextFormField类,状态管理为FormState

      //注意:获取FormState是 1.给Form添加Global<FormState>()得到的key 2.如果使用FormState.of(context)时,记得外面用Builder()组件包着
      //Form类的定义:
        // const Form({
        //   bool autoValidate=false,//是否每个输入框内容变化时自动校验,新版本已经变成autovalidateMode
        //   required Widget child,//子元素
        //   WillPopCallback onWillPop,//返回Flutter对象,当为true是允许返回之前路由,反则反之
        //   VoidCallback onchange,//每个输入框内容变化时触发该回调
        // })
      
      //FormField类定义
        // const FormField({
        //   FormFieldSetter<T> onSaved,//保存回调
        //   FormFieldValidator<T> validator,//校验回调
        //   bool autoValidate=false,//是否自动校验
        //   T initialValue,//初始值

        // })

        class FormValidateDemo extends StatefulWidget{
          @override
          _FormValidateDemo  createState()=> _FormValidateDemo();
        }
        class _FormValidateDemo extends State<FormValidateDemo>{
          GlobalKey _formKey = GlobalKey<FormState>();
          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('表单校验'),
              ),
              body:Container(
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      child: Form(
                        // autovalidateMode: AutovalidateMode.disabled,
                        key: _formKey,//唯一标识
                        //  autovalidateMode: AutovalidateMode.onUserInteraction,
                        onChanged: (){
                          print('change');
                        },
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              // autovalidate: true,
                              onSaved: (val){
                                print('onsaved:$val');
                              },
                              validator: (val){
                                // return val;
                                return val.trim().isNotEmpty ? null : "用户名不能为空";

                              },
                              initialValue: '陈铁柱',
                              autofocus: true,
                              decoration: InputDecoration(
                                labelText: '用户名:',
                                hintText: '请输入用户名',
                                border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.green))
                              ),
                            ),
                            TextFormField(
                              // autovalidate: true,
                              onSaved: (val){
                                print('onsaved:$val');
                              },
                              validator: (val){
                                return val.trim().isNotEmpty ? null : "用户名不能为空";

                              },
                              initialValue: '111',
                              autofocus: true,
                              decoration: InputDecoration(
                                labelText: '密码:',
                                hintText: '请输入密码',
                                border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.green))
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Builder(builder: (context){
                              return ElevatedButton(
                                child: Text('登录'),
                                onPressed: (){
                                  // FormState mystate = Form.of(context);//此处用Form.of(context)并不能获得所对应的FormState
                                  // // mystate.validate();
                                  // print('mystate:${mystate.validate()}');
                                  // print(Form.of(context).validate());
                                  FormState mystate = _formKey.currentState;
                                  print(mystate.validate());
                                },
                              );
                            }),
                          ),
                          Expanded(
                            child: Builder(builder: (context){
                              return ElevatedButton(
                                child: Text('重置'),
                                onPressed: (){
                                  //FormState实例三个方法validate()-全校验,save()-全保存,reset()-全重置
                                  FormState mystate = _formKey.currentState;
                                  mystate.reset();//重置
                                  //保存 mystate.save();
                                },
                              );
                            }),
                          ),

                        ],
                      ),
                    )
                  ],
                ),
              )
            );
          }
        }

    //3.5.3进度指示器
      //进度指示器分为2中:LinearProgressIndicator和CircularProgressIndicator 2种

      //3.5.3.1 LinearProgressIndicator
        // const LinearProgressIndicator({
        //   double value,//value为null时循环增加进度,不为null显示具体数值,范围[0,1]
        //   Color backgroundColor,//进度条背景颜色
        //   Animation<Color> valueColor,//进度条的颜色
        // })

      //3.5.3.2 CircularProgressIndicator
        // const CircularProgressIndicator({
        //   double value,//
        //   Animation<Color> valueColor,
        //   Color backgroundColor,
        //   this.strokeWidth=4.0//圆环宽度
        // })

        class ProgressIndicatorDemo extends StatelessWidget{

          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar:AppBar(
                title: Text('进度条'),
              ),
              body: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    LinearProgressIndicator(
                      backgroundColor: Colors.grey[200],
                      valueColor: AlwaysStoppedAnimation(Colors.blue),
                    ),
                    Container(
                      height: 20.0,
                    ),
                    LinearProgressIndicator(
                      backgroundColor: Colors.grey[200],
                      valueColor: AlwaysStoppedAnimation(Colors.blue),
                      value: .7,
                    ),
                    Container(
                      height: 20.0,
                    ),
                    CircularProgressIndicator(
                      backgroundColor: Colors.grey[200],
                      valueColor: AlwaysStoppedAnimation(Colors.blue),
                      strokeWidth: 20.0,
                    ),
                    Container(
                      height: 20.0,
                    ),
                    CircularProgressIndicator(
                      backgroundColor: Colors.grey[200],
                      valueColor: AlwaysStoppedAnimation(Colors.blue),
                      value: 1.0,
                      strokeWidth: 20.0,
                    ),
                  ],
                ),
              ),
            );
          }
        }
      
      //3.5.3.3自定义尺寸

        //一般使用尺寸限制类Widget设置尺寸例如:ConstrainedBox,SizeBox
        //注意:CicularProgressIndicator宽高不够会显示椭圆

        //自定义动画要点:1.AnimationController控制器 2._controller.forward()-播放动画 3._controller.disposed()-释放
          //注意:1.动画控制器需要vsync需要从SingleTickerProviderStateMixin混入得到 2.进度条value从_controller.value中获取
        class CustomProgressIndicatorDemo extends StatefulWidget  {
          @override
          _CustomProgressIndicatorDemo createState()=>_CustomProgressIndicatorDemo();
        }
        class _CustomProgressIndicatorDemo extends State<CustomProgressIndicatorDemo> with SingleTickerProviderStateMixin {
          
          AnimationController _controller;
          double _val=0.0;
          Animation<Color> _valColor;
         var _changeValColor;
         var _hadleValColor;
         //开关
         bool _flag = false;
          @override 
          void initState(){
            _controller = AnimationController(
              vsync: this,
              duration: Duration(seconds:10)
            );
            //初始动进度条颜色
          _valColor=AlwaysStoppedAnimation(Colors.blue);
          //变化进度条颜色
          _changeValColor = ColorTween(begin: Colors.grey, end: Colors.blue).animate(_controller);
          super.initState();
          _hadleValColor = _valColor;
          }
          @override
          void dispose(){
              _controller.dispose();
            super.dispose();
          }
          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('自定义进度条'),
              ),
              body: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[//ConstrainedBox这个暂时不知如何使用
                    SizedBox(
                      width: 200.0,
                      height: 20.0,
                      child: LinearProgressIndicator(
                        backgroundColor: Colors.yellow,
                        valueColor: _hadleValColor,
                        value: _val,
                      ),
                    ),
                    SizedBox(
                      width: 200.0,
                      height: 200.0,
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.yellow,
                        valueColor: _hadleValColor,
                        value: _val,
                      ),
                    ),
                    Container(
                      child: Text('进度:$_val'),
                    ),
                    Container(
                      padding: const EdgeInsets.all(20.0),
                      child: Container(
                        child: ElevatedButton(
                          child: Text('切换'),
                          onPressed: (){
                            if(_controller.isCompleted){//判断动画执行完毕后重置
                              _controller.reset();
                            }
                            setState(() {
                               _flag= !_flag;
                            });
                            if(_flag){
                              print(_flag);
                              setState(() {
                                _hadleValColor=_changeValColor;
                                
                              });
                              _controller.addListener((){
                                print('listener');
                                setState(() {
                                  _val=_controller.value;
                                });
                              });
                              _controller.forward();
                            }else{
                              setState(() {
                                _changeValColor= _valColor;
                                _val=0.0;
                              });
                              // _controller.dispose();
                            }
                          },
                        ),
                      ),
                    )
                    
                  ],
                ),
              ),
            );
          }
        }

      //3.5.3.4自定义进度条样式 可以通过CustomPainer自定义,这以后学习自定义组件后再学习

//第四章 布局类组件

  //4.1布局类组件简介
    
    //4.1.1组件类型:主要分类三大类
      //1.非容器组件基类
        //widget:LeafRenderObjectWidget
        //说明:非容器组件基类
        //用途:Widget树的叶子节点(或者说末端节点),用于没有子节点的Widget,通常基础组件都属于这一类,例如Text,Image,ElevatedButton,TextFiled

      //2.单子组件基类
        //widget:SingleChildRenderObjectWidget
        //说明:单子组件基类
        //用途:包含一个子Widget,l例如Container,Center等

      //3.多子组件基类
        //Widget:multiChildRenderObjectWidget
        //说明:多子组件基类
        //用途:包含多个子类,例如Row,Column,Stack等

      //注意:1.布局类组件主要继承于SingleChildRenderObjectWidge和mulChildRenderObjectWidget
          //2.RenderObjectWidget是最终布局和渲染的对象

    //4.2布局原理与约束

      //4.2.1 flutter2种布局模型
        //1.基于RenderBox的盒模型
        //2.基于Sliver(RenderSlive)的按需加载列表模型

        //盒模型布局流程:
          //1.上层组件向下层组件传递限制条件 例如限制宽高0-100
          //2.下层组件确定自己的大小,然后告诉上层组件 例如设置200*200
          //3.根据下层组件的反馈和自己的约束条件确定子组件相对自身的偏移和自身大小 例如:上述子组件宽高超过限制条件最大值,则取最大值,反则反之

        //盒模型特点:
            //1.组件渲染对象都继承自RenderBox类
            //2.布局过程中父级传递给子级的约束信息由BoxConstraints描述

      //4.2.2 BoxConstraints-一般用于约束盒子中的constraints属性
        //BoxConstraints是盒模型父组件传递给子组件的约束信息
        // const BoxConstraints({
        //   this.minWidth=0.0,//最小宽度
        //   this.maxWidth=double.infinity,//最大宽度无穷大
        //   this.minHeight=0.0,//最小高度
        //   this.maxHeight=double.infinity,//最大高度

        //   //BoxConstraints还提供了一些构造函数
        //     //1.BoxConstraints.tight()-生成固定宽高的限制
        //     //2.BoxConstraints.expand()-生产一个尽可能大的用以填充另一个容器的Constraints()
          
        //   //注意:一个组件对子组件取消约束或者说子组件的宽高由自己决定指:约束条件的最小宽高为0,最大宽高为无限大
        // })

      //4.2.3 ConstrainedBox

        //用于对子组件也就是该组件的child组件添加额外的限制
        class ConstrainedBoxContainerDemo extends StatelessWidget {
          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('约束盒子')
              ),
              body: Container(
                // height: 1000.0,
                // width: 1000.0,此处有宽高限制盒子就范围就不生效

                decoration: BoxDecoration(color: Colors.blue),
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: 5000.0,
                    maxWidth: 100.0
                  ),
                  child: Container(
                    height: 100.0,
                    width:700.0,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      
                    ),
                    // child: Text('6666'),
                  ),
                ),
              ),
            );
          }
        }

      //4.2.4 SizedBox

        class SizeBoxDemo extends StatelessWidget {
          final Widget _blueBox = DecoratedBox(
                          decoration: BoxDecoration(color: Colors.blue),
                        );
          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('BoxSize'),
              ),
              body: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: SizedBox(//相当于 constrainedBox(child:_blueBox,constrainst:BoxConstraints.tighfor(width:80.0,height:80.0))
                                      //相当于...constrainst:BoxConstraints(minHeight:80.0,maxHeight:80.0,minWidth:80.0,maxHeight:80.0)
                        width: 80.0,
                        height: 80.0,
                        child: _blueBox
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        }

      //4.2.5 多重约束:三种情况
        //1.都只写minHeight和minWidth时,取对应最大的
        //2.都只写maxHeight和maxWidth时,取对应最小的
        //3.minHeight和maxHeigth,width同,每个限制盒子都有时
            //(1).有交集多重有交集的,取交集的值 例如height高度为[100,200][0,100]则取100
            //(2).height如果没有交集则取范围相对较大的区间的最小值 例如[0,50] [100,200] 取100
            //(3)width 如果区间没有交集 取较小区间的最大值 [0,50][100,200] 取50
            //(4)如果多个区间都有交集,且child宽高也在这个范围则取child宽高
        class MultialConstraints extends StatelessWidget {

           final Widget _blueBox = DecoratedBox(
              decoration: BoxDecoration(color: Colors.red),
            );
          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('多重约束'),
              ),
              body: Container(
                child: Container(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      // minHeight: 100.0,
                      // maxHeight: 200.0,
                      // minWidth: 20.0,
                      // maxWidth:200.0
                      minHeight: 100.0,
                      maxHeight: 400.0,
                      minWidth: 100.0,
                      maxWidth:200.0
                    ),
                    child: ConstrainedBox(
                      constraints:BoxConstraints(
                        // minHeight: 10.0,
                        // maxHeight: 100.0,
                        // minWidth: 200.0,
                        // maxWidth:1000.0,
                        minHeight: 10.0,
                        maxHeight: 50.0,
                        minWidth: 200.0,
                        maxWidth:400.0,
                      ),
                      child: Container(
                        width:300.0,
                        height: 300.0,
                        child: _blueBox,
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
        }

      //4.2.6 UnconstrainedBox
        //下列例如如果没有UnconstrainedBox,对应显示宽高应该为90*100,而实际加了后为90*20
        //虽然显示为90*20 但是,高度范围还剩80也是站位置的(只是不显示)
        class UnconstrainedBoxDemo extends StatelessWidget {
          final Widget _blueBox = DecoratedBox(
              decoration: BoxDecoration(color: Colors.red),
            );
          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('解除限制组件'),
              ),
              body: Container(
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 20.0,
                      color: Colors.green,
                    ),
                      Container(
                        width: 100.0,
                        height: 100.0,
                        alignment: Alignment.bottomLeft,
                        color: Colors.black,
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minHeight: 100.0,
                            minWidth: 60.0
                          ),
                          child: UnconstrainedBox(
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                minHeight: 20.0,
                                minWidth: 90.0
                              ),
                              child:_blueBox ,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            );
          }
        }

      //4.2.7 其他约束类容器

        //AspectRatio,LimitedBox,FrationallySizedBox

    //4.3线性布局(Row,Column)

      //4.3.1 主轴(mainAxisAlignment)和纵轴(crossAxisAlignment):如果布局是水平方向,则主轴MainAxisAlignment是水平方向;若布局是垂直方向,则主轴是垂直方向

      //4.3.3 Row

        //定义如下
        // const mywidget = Row({
        //   // ...,此处省略不重要属性
        //   TextDirection textDirection,//取值:TextDirection.(ltr,rtl)水平方向子组件的布局顺序,子组件文本方向(默认中英从左往右,阿拉伯从右往左)
        //   MainAxisSize mainAxisSize= MainAxisSize.max,//mainAxisSize.max尽可能的占据水平空间
        //   MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,//1.取值.start,.end,center 2.若textDirection.ltr,则.start从左往右;若textDirection.rtl,则.statr从右往左 3.前面2条前提是在mainAxisSize.Max
        //   CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.center,//和mainAxisAlignment类似
        //   VerticalDirection verticalDirection = VerticalDirection.down,//纵轴的对齐方向,(1).VerticalDirection.down,crossAxisAlignment.strat时顶部对齐;(2).VerticalDirection.up,crossAlignment.up时底部对齐和textDirection类似
        //   List<Widget> children = const <Widget>[]
        // });

        class RowDemoClass extends StatelessWidget {

          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar:AppBar(
                title: Text('Row布局'),
              ),
              body:Column(

                children: <Widget>[
                  Container(//从右显示:can we go to play balltoday is wednesday
                    color: Colors.red,
                    child: Row(
                      textDirection:TextDirection.rtl,
                      mainAxisAlignment:MainAxisAlignment.start,
                      children: [
                        Text('today is wednesday'),
                        Text('can we go to play ball')
                      ],
                    ),
                  ),
                  Container(// 包含rtl的倒序显示 can we go to play balltoday is wednesday
                    color: Colors.blue,
                    child: Row(
                      textDirection:TextDirection.rtl,
                      mainAxisAlignment:MainAxisAlignment.end,
                      children: [
                        Text('today is wednesday'),
                        Text('can we go to play ball')
                      ],
                    ),
                  ),
                  Container(//注意高度参照于Row子组件的最大高度
                    color: Colors.yellow,
                    child: Row(
                      textDirection:TextDirection.rtl,
                      mainAxisAlignment:MainAxisAlignment.start,
                      children: [
                        Text('today is wednesday'),
                        Text('can we go to play ball')
                      ],
                    ),
                  ),
                  Container(
                    color: Colors.green,
                    child: Row(
                      verticalDirection:VerticalDirection.down,
                      crossAxisAlignment:CrossAxisAlignment.start,
                      children: [
                        Text('today is wednesday',style: TextStyle(fontSize: 30.0,color:Colors.black12),),
                        Text('can we go to play ball')
                      ],
                    ),
                  ),
                ],
              )
            );
          }
        }

      //4.3.4 Column 和Row 差不多 只是要注意 使用CrossAxisAlignment.center时,居中是参照最大子组件的宽度居中的

      //4.3.5 如果Column里面套Row或者Column里面套Row,只有最外面的Column或者Row生效
       
        class SpecialCaseDemo extends StatelessWidget {

          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('特殊情况'),
              ),
              body: Container(
                color: Colors.green,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      // color: Colors.blue,
                      // child: Column(//没有占满,只是自身大小
                      //   mainAxisSize: MainAxisSize.max,
                      //   children: [
                      //     Text('we will go back in May')
                      //   ],
                      // ),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Container(
                            color: Colors.blue,//没有占满,只是自身大小
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(' buy a house in May')
                              ],
                            )
                          )
                        ],
                      ),
                    )
                  ],
                ),  
                width: 200.0,
              ),
            );
          }
        }

    //4.4 flex布局:类似h5中弹性盒布局和Android中flexboLayout

      //4.4.1 Flex:1.Column 和Row 都继承自Flex 2.Flex继承自multiChildRenderObjectWidget,对应的RenderObject为RenderFlex

          // Flex({
          //   ...,
          //   required this.direction,//弹性布局方向,Row默认水平方向,Column默认垂直方向,取值:Axis.horizontal,.vertical
          //   List<Widget> children = const <Widget>[]
          // });
      //4.4.2 Expanded :1.只能作为Flex的子元素,否则会报错 2.弹性系数,若为0和null则没有弹性,不会伸展占据空间;若不为0,则主轴方向按照比例分割占据空间

        // Expanded({
        //   int flex= 1,
        //   required Widget child
        // })

        class FlexLayoutDemo extends StatelessWidget {

          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('弹性盒布局'),
              ),
              body: Container(
                color: Colors.pink,
                child: Column(
                  children: [
                    //水平方向排列
                    Flex(
                      direction: Axis.horizontal,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Container(
                            color: Colors.blue,
                            child: Text('一个'),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            color: Colors.green,
                            child: Text(' seconds'),
                          ),
                        )
                      ],
                    ),
                    Container(
                      color: Colors.white,
                      height: 20.0,
                    ),
                    Container(
                      width: 300.0,
                      height: 300.0,
                      child: Flex(
                        direction: Axis.vertical,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Container(
                              color: Colors.blue,
                              child: Text('一个'),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Container(
                              color: Colors.green,
                              child: Text(' seconds'),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        }

    //4.5 流式布局:超出Widget显示范围自动折行的布局称为流式布局,类似h5中的div超出自动换行
        //前言:若Row子组件超出widget则会溢出报错,因为Row只有一行,这时候就要用到流式布局

          class TestRowOut extends StatelessWidget {

            @override
            Widget build(BuildContext buildContext){
              return Scaffold(
                appBar: AppBar(
                  title: Text('测试ROW超出'),
                ),
                body: Container(
                  child: Column(
                    children: [
                      Row(
                        children: [//会超出报错
                          Text('测试成功'*100)
                        ],
                      )
                    ],
                  ),
                ),
              );
            }
          }

        //4.5.1 Wrap
          //定义:
            // Wrap({
            //   // ...
            //   this.direction = Axis.horizontal,
            //    this.crossAxisAlignment=WrapAlignment.start,
            //   this.textDirection,
            //   this.verticalDirection=VerticalDirection.down,//前面四个属性和Row,column中国属性类似的
            //   this.alignment=WrapAlignment.start,//主轴方向的对齐方式
            //   this.spacing = 0.0,//主轴方向Widget的空隙
            //   this.runAlignment=WrapAlignment.start,//纵轴方向的对齐方式
            //   this.runSpacing=0.0,//纵轴方向Widget的间隔
            //   List<Widget> children=const <Widget>[]
            // })

            //注意 alignment和textDirection,verticalDirection属性有点重合,后2个权重比较高

            class WrapDemoTest extends StatelessWidget {

              @override
              Widget build(BuildContext buildContext){
                return Scaffold(
                  appBar: AppBar(
                    title: Text('wrap test'),
                  ),
                  body: Container(
                    child: Container(
                      width: 300.0,
                      height: 300.0,
                      color: Colors.black26,
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        textDirection: TextDirection.rtl,
                        verticalDirection: VerticalDirection.up,
                        direction: Axis.vertical,
                        spacing: 30.0,
                        runSpacing: 30.0,
                        children: [
                          Container(
                            width: 80.0,
                            height: 80.0,
                            color: Colors.red,
                          ),
                          Container(
                            width: 80.0,
                            height: 80.0,
                            color: Colors.blue,

                          ),
                          Container(
                            width: 80.0,
                            height: 80.0,
                            color: Colors.green,
                          ),
                          Container(
                            width: 80.0,
                            height: 80.0,
                            color: Colors.green,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }
            }

        //4.5.2 Flow :自定义布局策略,通常情况能弄Wrap就使用Wrap,因为Flow相对复杂

            //Flow 优点:1.性能好,重绘时使用了矩阵转换,并没有实际调整组件位置 2.灵活
              //这个有些复杂,后面再细看

    //4.6 层叠布局Stack,Positioned
          //和web中绝定定位,android中Fram布局相似,子组件可以根据父组件四个角位置确定自身位置
          //Stack允许堆叠,Positoned确定位置

        //4.6.1 Stack 

          // Stack({
          //   this.alignment = AlignmentDirectional.topStart,//决定没有使用Positioned子组件或者部分定位(只在横轴或者纵轴)的对齐方式,
          //   this.textDirection,//和row,column一样用于alignment对齐参考
          //   this.fit = StackFit.loose,//决定没有定位子组件的大小,参数.loose-自身大小,.expand-扩展到Stack大小
          //   this.clipBehavior= Clip.hardEdge,//超出Stack如何裁剪,.hardEdge不用抗锯齿裁剪
          // })

        //4.6.2 Positioned 

          // Positioned({
          //   Key key,
          //   this.left,
          //   this.right,
          //   this.top,
          //   this.bottom,//left,right,top,bottom表示距离四边的距离
          //   this.width,
          //   this.height,//此处的height和width用于协助计算水平垂直方向的位置,例如水平方式:只需要 width 和left或者right中一个即可,三个同时写时会报错
          //   required Widget child
          // })

          class StackTestDemo extends StatelessWidget {
            @override
            Widget build(BuildContext buildContext){
              return Scaffold(
                appBar: AppBar(
                  title: Text('position test'),
                ),
                body: Container(
                  child: ConstrainedBox(//一定要加现在盒子,否则没有参照体系
                    constraints: BoxConstraints.expand(),
                    child: Stack(
                    alignment: Alignment.topRight,
                    textDirection: TextDirection.rtl,
                    fit: StackFit.loose,
                    children: [
                      Container(
                        width: 250.0,
                        height: 100.0,
                        color: Colors.red,

                      ),
                      Container(
                        width: 100.0,
                        height: 100.0,
                        color: Colors.blue,
                      ),
                      Positioned(
                        bottom:100.0,
                        left: 30.0,
                        child: Container(
                          width: 100.0,
                          height: 100.0,
                          color: Colors.green,
                        )
                        
                      )
                    ],
                  ),
                  ),
                ),
              );
            }
          }

    //4.7 对齐与相对定位(Align):Stack和Positioned一般用于对个子组件相对一个父组件的定位,如果只有一个子组件定位用Align要简单一些

      //4.7.1 Align
        // const Align({
        //   Key key,
        //   this.alignment = Alignment.center,
        //   this.widthFactor,
        //   this.heightFactor,//Align (特别注意当align外层组件没有指定宽高的前提)组件的宽高的缩放因子,用子组件宽高*缩放因子最后得到Align组件宽高,如果值为null,会尽可能占空间
        //   Widget child,
        // })

        class AlignTestDemo extends StatelessWidget {

          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('align test'),
              ),
              body: Container(
                child: Container(
                  // width: 200.0,
                  // height: 200.0,
                  color: Colors.blue,
                  child: Align(
                    alignment: Alignment.center,
                    widthFactor: 2,
                    heightFactor: 2,
                    child: Container(
                      width: 100.0,
                      height: 100.0,
                      color: Colors.green,
                      child: Text('test demo'),
                    ),
                    // child: FlutterLogo(
                    //   size: 100,
                    // ),
                  ),
                ),
              ),
            );
          }
        }

        //关于Alignment属性的值
          //1.Alignment(x,y)x,y取值范围一般都为[-1,1]中心点为(0,0)
          //2.子元素具体坐标公式 (childWidth*x/2+childWidth/2,childHeight*y/2+childHeight/2),有时x可能取到2(这个可能有问题尽量少用)
          //3.Alignment.center==Alignment(0,0) Alignment.bottomRight==Alignment(-1,-1)

          class AlignmentTestDemo extends StatelessWidget{

            @override
            Widget build(BuildContext buildContext){
              return Scaffold(
                appBar:AppBar(
                  title: Text('alignment test demo '),
                ),
                body: Container(
                  child: Container(
                    width: 500.0,
                    height: 500.0,
                    color: Colors.blue,
                    child: Align(
                      alignment: Alignment(1,0.0),
                      child: Container(
                        width: 100.0,
                        height: 100.0,//(X,Y) X=100*1/2+100/2=100 Y=100*0/2+100/2=50
                        color: Colors.green,
                        child: Text('test alignment demo'),
                      ),
                      // child:FlutterLogo(
                      //   size: 60,//(x,y) x=60*1/2+60/2=60 x=60*0/2+60/2=30
                      // )
                    ),
                  ),
                ),
              );
            }
          }

        //FractionalOffset 
          //FractionalOffset 继承于Alignment,唯一不同是坐标原点为左顶点

          class FractionalOffsetTestDemo extends StatelessWidget {

            @override
            Widget build(BuildContext buildContext){
              return Scaffold(
                appBar: AppBar(
                  title: Text('fractionalOffset'),
                ),
                body: Container(
                  color: Colors.green,
                  child: Container(
                    child: Align(
                      alignment: FractionalOffset(0.5,0.5),//范围[0,1]
                      child: Container(
                        width: 50.0,
                        height: 50.0,
                        color: Colors.yellow,
                      ),
                    )
                  ),
                ),
              );
            }
          }

      //4.7.2 Stack,Positioned和Align 对比

        //共同点:1.都是相对于父元素定位
        //区别:1.参考系统不同:Positioned参照四个顶点,而align先要根据Alignment确定原点,最终偏移要通过Alignment偏移计算而得
          //  2.Stack可以有多个子元素堆叠,而Align只要一个子元素

      //4.7.3 Center:实际上Center继承自Align,只是默认1.alignment:Alignment.center 2.widthFactor,heightFactor为null

            //组件定义
              // const class Center extends Align {
              //   const Center({
              //     Key key,
              //     double widthFactor,
              //     double heightFactor,
              //     Widget child
              //   }):super(key:key,widthFactor: widthFactor,heightFactor: heightFactor,child: child);
              // }

              //注意:1.Center与Align只少了Alignment,而默认Alignment为Alignment.center  所以默认是居中
                //  2.widthFactor,heightFactor默认为null,当为null时组件的宽高会占尽可能多的空间,下面举例看一下

                class DifferentBox extends StatelessWidget {

                  @override
                  Widget build(BuildContext buildContext ){
                    return Scaffold(
                      appBar: AppBar(
                        title: Text('差别对比'),
                      ),
                      body: Container(
                        child: Column(
                          children: [
                            Container(
                              // width: 100.0,
                              // height: 100.0,
                              color: Colors.pink,
                              child: Center(
                                child: Text('first box'),
                              ),
                            ),
                            Container(
                              // width: 100.0,
                              // height: 100.0,
                              color: Colors.blue,
                              child: Center(
                                widthFactor: 1.0,
                                heightFactor: 1.0,
                                child: Text('second box'),//若Center的父widget都没有指定宽高时,widthFactor,heightFactor都为null时会向上级寻找最近确定宽度父级宽度并继承,而不为null的话则参照爷爷组件最大宽度居中(偏移)
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }
                }

    //4.8 LayoutBuilder,AfterLayout

          //4.8.1 LayoutBuilder:通过LayoutBuilder在布局过程中拿到父组件的约束信息,根据约束信息动态布局,例如:做一个响应式的组件,当Column宽度小于200时子组件分一列展示,否则分2列展示

            //灵活布局
            class FlexCustomLayout extends StatelessWidget {
              
              final List<Widget> children;
              const FlexCustomLayout({Key key,@required this.children}):super(key:key);
              
              @override
              Widget build(BuildContext buildContext){
               return LayoutBuilder(
                 builder: (BuildContext context,BoxConstraints  constrains){
                   print('length:${children.length}');
                   List<Widget> _children = <Widget>[];
                    if(constrains.maxWidth>200){
                      for(int i=0;i<children.length;i+=2){
                        if(i+1<children.length){
                          _children.add(
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [children[i],children[i+1]],
                            )
                          );
                        }else{
                          _children.add(children[i]);
                        }
                        
                      }
                      return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: _children,
                          crossAxisAlignment: CrossAxisAlignment.start,
                        );
                    }else{
                      return Column(children: children,mainAxisSize: MainAxisSize.min);
                    }
                   
                 },
               );
              }
            }
            //打印布局的约束信息
            class PrintConstrainsInfo extends StatelessWidget {
              const PrintConstrainsInfo({Key key,@required this.child}):super(key:key);
              final Widget child;
              @override
              Widget build(BuildContext buildContext){
                return LayoutBuilder(
                  builder: (BuildContext buildContex,BoxConstraints constraints){
                    
                    //assert 在release版本中assert会被清除
                    assert((){
                      print('child info:$constraints');
                      return true;
                    }());
                    return child;
                  },
                );
              }
            }
            class ShowLayoutDemo extends StatefulWidget {
              @override
              _ShowLayoutDemo createState()=> _ShowLayoutDemo();
            }
            class _ShowLayoutDemo extends State<ShowLayoutDemo> {
              List<Widget> _childrenDemo=List<Widget>();
              @override
              void initState(){
                bool _flag=false;
                for(int index=0;index<3;index++){
                  if(index.isOdd){
                    _flag=!_flag;
                  }
                  print(_flag);
                  _childrenDemo.add(
                    Container(
                      width: 100.0,
                      height: 100.0,
                      color: _flag?Colors.blue:Colors.green,
                    )
                  );
                  
                }
                super.initState();
              }
              @override
              Widget build(BuildContext buildContext){
                return Scaffold(
                  appBar: AppBar(
                    title: Text('flex layout demo'),
                  ),
                  body: Container(
                    color: Colors.pink,
                    child: Column(
                      children: [
                        SizedBox(
                          width: 300,
                          child: FlexCustomLayout(children: _childrenDemo),
                        ),
                        SizedBox(
                          width: 150,
                          child: FlexCustomLayout(children: _childrenDemo),
                        ),
                        PrintConstrainsInfo(child: Text('约束信息R'),)
                      ],
                    ),
                  ),
                );
              }
            }

          //4.8.2 AfterLayout --作者自定义组件:后面再详细介绍
            //作用:1.获取组件大小和相对屏幕的坐标 2.获取组件相对于某个组件的坐标
              // AfterLayout(
              //   callback: (RenderAfterLayout ral) {
              //     print(ral.size); //子组件的大小
              //     print(ral.offset);// 子组件在屏幕中坐标
              //   },
              //   child: Text('flutter@wendux'),
              // )

              // Builder(builder: (context) {
              //   return Container(
              //     color: Colors.grey.shade200,
              //     alignment: Alignment.center,
              //     width: 100,
              //     height: 100,
              //     child: AfterLayout(
              //       callback: (RenderAfterLayout ral) {
              //         Offset offset = ral.localToGlobal(
              //           Offset.zero,
              //           // 传一个父级元素
              //           ancestor: context.findRenderObject(),
              //         );
              //         print('A 在 Container 中占用的空间范围为：${offset & ral.size}');
              //       },
              //       child: Text('A'),
              //     ),
              //   );
              // }),

          //4.8.3 flutter build和layout是交错执行的,并不是严格按照build=>layout顺序,例如build遇到layoutBuilder


  
//第五章容器类组件

  //5.1 填充 Padding

    //5.1.1 Padding :可以给子组件留白,类似边距

      //定义
        // Padding({
        //   // ...
        //   EdgetInsetsGemetry padding,//经常用的EdgeInsets继承于EdgetInsetsGemetry
        //   Widget child,//
        // })

    //5.1.2 EdgetInsets

      //EdgeInsets常用方法:
        //1.EdgeInsets.fromLTRB(double left,double top,double right,double bottom):分别指向四个方向的填充
        //2.add(double value):四个方向填充相同的数值
        //3.only({left,top,right,bottom}):具体某个方向的填充(可以同时允许多个方向)
        //4.symmetric({vertical,horizontal}):设置对称方向的填充 vertical指top,bottom方向(垂直方向)
      
      //例子:
       
        class EdgeInsetsDemo extends StatelessWidget {

          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('edgeInsets demo test'),
              ),
              body: Container(
                color: Colors.pink,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10.0,10.0,10.0,10.0),
                      child: Container(
                        width: 50.0,
                        height: 50.0,
                        color: Colors.blue,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 50.0,
                        height: 50.0,
                        color: Colors.green,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0,right: 20.0),
                      child: Container(
                        width: 50.0,
                        height: 50.0,
                        color: Colors.yellow,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal:40.0),
                      child: Container(
                        width: 50.0,
                        height: 50.0,
                        color: Colors.green,
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        }

    //5.2装饰容器(DecoratedBox)

      //5.2.1 decoratedBox 可以在子组件绘制前或者后绘制一些装饰,例如背景,渐变,边框等

        //定义如下:
          // DecoratedBox({
          //   Decoration decoration,//绘制的装饰
          //   DecorationPosition position = DecorationPosition.background,//在哪里绘制Decoration,取值有:1.background-组件之后绘制(即背景) 2.foreground-组件之前绘制(即前景)
          //   Widget child,
          // })

      //5.2.2 BoxDecoration:为Decoration的一个子类,实现了常用元素的绘制

        // BoxDecoration({
        //   Color color,//颜色
        //   DecorationImage image,//图片
        //   BoxBorder border,//边框
        //   BorderRadiusGeometry borderRadius,//圆角
        //   List<BoxShadow> boxShadow,//阴影可以指定多个
        //   Gradient gradient,//渐变取值:LinearGradient,RadialGradient,SweepGradient
        //   BlendMode backgroundBlendMode,//背景混合模式
        //   BoxShape shape=BoxShape.rectangle,//形状


        // })

        class BoxDecorationDemo extends StatelessWidget {

          @override
          Widget build(BuildContext buildContext){
            return Scaffold(
              appBar: AppBar(
                title: Text('decoration box'),
              ),
              body: Container(
                child: Column(
                  children: [
                    DecoratedBox(
                      // position: DecorationPosition.foreground,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(4.0),
                        gradient: LinearGradient(colors: [Colors.yellow,Colors.blue,Colors.black]),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black54,
                            offset: Offset(2.0,2.0),
                            blurRadius: 4.0
                          ),
                          BoxShadow(
                            color: Colors.purple,
                            offset: Offset(0.0,0.0),
                            blurRadius: 8.0
                          )
                        ]
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Text('decoratedBox test'),
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        }
    //
    class DecoratedBoxTest1 extends StatelessWidget {
      @override
      Widget build(BuildContext context){
        return Scaffold(
          appBar: AppBar(title: Text('修饰盒子')),
          body: Center(
            child: Container(
              child: DecoratedBox(
                position: DecorationPosition.background,
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(4.0),
                  gradient: LinearGradient(colors: [Colors.blue,Colors.yellow]),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      offset: Offset(2.0,2.0),
                      blurRadius: 4.0
                    ),
                    BoxShadow(
                      color: Colors.red,
                      offset: Offset(6.0,6.0),
                      blurRadius: 8.0
                    )
                  ],
                  shape: BoxShape.rectangle
                ),
               
                child: Padding(
                  padding: const EdgeInsets.all(40),
                  child: Text('6家啊啊'),
                ),
              ),
            ),
          ),
        );
      }
    }

    //5.3 Transform :对其组件用Matrix4(4d矩阵)来实现一些特效

    class Matrix4ContainerDemo extends StatelessWidget {
      @override
      Widget build(BuildContext buildContext){
        return Scaffold(
          appBar: AppBar(
            title: Text(''),
          ),
          body: Center(
            child: Container(
              width: 100.0,
              height: 30.0,
              child: DecoratedBox(
                position: DecorationPosition.background,
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(4.0),
                  gradient: LinearGradient(colors: [Colors.yellow,Colors.deepPurple]),
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0.0,0.0),
                      color: Colors.red,
                      blurRadius: 6.0
                    )
                  ]
                ),
                child: Transform(
                  alignment: Alignment.topRight,
                  transform: Matrix4.skew(-0.6,0.88),
                  child: Container(
                    color: Colors.greenAccent,
                    child: Center(child: Text('let us go'),),
                  ),
                ),
              ),
            ),
          ),
        );
      }
    }
    class TransformDemoTest1 extends StatelessWidget {
      Widget build(BuildContext context){
        return Scaffold(
          appBar: AppBar(
            title: Text('transform test'),
          ),
          body: Container(
            color: Colors.green,
            child: Center(
              child: Column(
                children: [//无论transform做如何变化,所占大小都不变
                  // DecoratedBox(
                  //   position: DecorationPosition.background,
                  //   decoration: BoxDecoration(
                  //     color: Colors.blueAccent,
                  //     gradient: LinearGradient(
                  //       colors: [Colors.black12,Colors.orangeAccent]
                  //     )
                  //   ),
                  //   child: Transform.translate(
                  //     offset: Offset(2.0,2.0),
                  //     child: Text('let us go',style: TextStyle(color: Colors.blue),),
                  //   ),
                  // ),
                  // DecoratedBox(
                  //   position: DecorationPosition.background,
                  //   decoration: BoxDecoration(
                  //     color: Colors.blueAccent,
                  //     gradient: LinearGradient(
                  //       colors: [Colors.black12,Colors.orangeAccent]
                  //     )
                  //   ),
                  //   child: Transform.rotate(
                  //     angle: math.pi/2,//旋转90度
                  //     child: Text('let us go',style: TextStyle(color: Colors.red)),
                  //   ),
                  // ),
                  DecoratedBox(
                    position: DecorationPosition.background,
                    decoration: BoxDecoration(
                      color: Colors.blueAccent,
                      gradient: LinearGradient(
                        colors: [Colors.black12,Colors.orangeAccent]
                      )
                    ),
                    child: Transform.scale(
                      scale: 2.5,
                      child: Text('let us go',style: TextStyle(color: Colors.purple)),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }
    }

    //rotatedBox-作用相当于 transform.rotate,但是rotatedBox是作用月布局阶段,其效果是占用位置的,而transform.rotate作用月绘制阶段,不占用空间
    class RotatedBoxTest1 extends StatelessWidget {
      Widget build(BuildContext context){
        return Scaffold(
          appBar: AppBar(
            title: Text('test1l'),
          ),
          body: Center(
            child: Row(
              children: [
                DecoratedBox(
                  decoration: BoxDecoration(
                    color: Colors.red,
                    gradient: LinearGradient(
                      colors: [Colors.blue,Colors.yellow]
                    )
                  ),
                  position: DecorationPosition.background,
                  child: RotatedBox(
                    quarterTurns: 1,//4/1圈，旋转90度
                    child: Text('les us  go'),
                  ),
                ),
                Text('哈哈哈')
              ],
            ),
          ),
        );
      }
    }

  //5.4 容器组件 

    // Container({
    //   this.alignment,//
    //   this.padding,//容器补白,属于decoration的装饰范围
    //   Color color,//背景颜色
    //   Decoration decoration,//背景装饰
    //   Decoration foreroundDecoration,//前景装饰
    //   double width,//宽
    //   double height,//高
    //   BoxConstraints constraints,//容器大小限制,
    //   this.margin,//容器补白,不包括decoration装饰范围,
    //   this.transform,//变换
    //   this.child,//...
    // })

    //注意点:1.width,height和constraints都能决定容器大小,当二者都存在时width,height优先级更高
    //2.decoration和color不能同时存在,否则会报错

      class ContainerValueTest extends StatelessWidget {
        Widget build(BuildContext context){
          return Scaffold(
            appBar: AppBar(
              title: Text('container test '),
            ),
            body: Center(
              child: Container(
                // alignment Alignment.topLeft
                margin: EdgeInsets.only(top:50.0,bottom: 50.0),
                constraints: BoxConstraints.tightFor(width: 200.0,height: 150.0),
                decoration: BoxDecoration(
                    gradient: RadialGradient(
                      colors:[Colors.red, Colors.orange],
                      center:Alignment.topLeft,
                      radius: 0.98
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black12,
                        offset: Offset(2.0,2.0),
                        blurRadius: 4.0
                      )
                    ]
                ),
                transform: Matrix4.rotationZ(.4),//沿y轴倾斜
                alignment: Alignment.center,//卡片内文字居中
                child: Text(
                  'container',style: TextStyle(
                    color: Colors.blue,
                    fontSize: 40.0
                  ),
                ),
              ),
            ),
          );
        }
      }

  //5.5 裁剪(clip)

    //5.5.1剪裁类组件
      //1.clipOval:(1)子组件为正方形是裁剪为圆形 (2)子组件为矩形时裁剪为椭圆形
      //2.clipRRect:裁剪为圆角矩形
      //3.clipRect:裁剪子组件溢出部分
      //4.clipPath:自定义路径裁剪

      class ClipTestDemo1 extends StatelessWidget {
        Widget build(BuildContext context){
          Widget imageDemo = Image.asset('image/back.png',width: 60.0,);
          return Scaffold(
            appBar: AppBar(
              title: Text('clip test'),
            ),
            body: Center(
              child: Column(
                children: [
                  imageDemo,
                  ClipOval(
                    child: imageDemo,
                  ),
                  ClipRRect(
                    child: imageDemo,
                    borderRadius: BorderRadius.circular(5.0)
                  ),
                  Row(
                    children: [
                      Align(
                        widthFactor: .5,//显示宽度为实际宽度一般故而会超出
                        child: imageDemo,
                      ),
                      Text('try to change',style:TextStyle(color: Colors.red,fontSize: 60.0))
                    ],
                  ),
                  Row(
                    children: [
                      ClipRect(
                        child: Align(
                          widthFactor: .5,//显示宽度为实际宽度一半故而会超出,超出部分被裁剪(参照物其实为原本宽高)
                          child: imageDemo,
                        ),
                      ),
                      Text('try to change',style:TextStyle(color: Colors.red,fontSize: 60.0))
                    ],
                  )
                ],
              ),
            ),
          );
        }
      }

    //5.5.2自定义裁剪
    class GetCliperDemo extends CustomClipper<Rect>{
      @override
      Rect getClip(Size size){
        return Rect.fromLTWH(size.width/4, size.height/4, size.width/2, size.height/2);
      }
      @override
      bool shouldReclip(CustomClipper<Rect> oldClipper){
        return false;//第一后就重新剪裁 true-是 false-否
      }
    }
    class CustomClipperDemo extends StatelessWidget {
      Widget build(BuildContext context){
        Widget imageDemo = Image.asset('image/back.png',width: 60.0,);
        return Scaffold(
          appBar: AppBar(
            title: Text('test clipper'),
          ),
          body: Container(
            width:200.0,
            height: 200.0,
            color:Colors.orange,
            child: ClipRect(
              clipper: GetCliperDemo(),
              child: imageDemo,
            ),
          ),
        );
      }
    }


    //5.6 空间适配

      //5.6.1 FittedBox
        //FittedBox作用:1.通常情况下父组件会将自身的宽高作为约束传递给子组件遵守,但是如果父组件宽高确定的话就有可能超出,那么适配时就会用到FittedBox;
                      //2.有些情况子组件本身占不满父组件,而需求是占满父组件,这时候也会用到改组件
                      //总而言之该组件的作用就是适配空间

          // FittedBox({
          //   Key key,//身份证
          //   this.fit = BoxFit.contain,//适配方式
          //   this.alignment = Alignment.center,//对齐方式
          //   this.clipBehavior = Clip.none,//是否裁切
          //   Widget child,
          // })

        //原理:1.FittedBox可以取消父组件对子组件的限制,即限制为 0<=width<=double.infinity,0<=height<=double.infinity
              //2.FittedBox在子组件布局完成后可以获取子组件的真实大小
              //3.FittedBox知道子组件真实大小,也知道父组件约束条件,FixttedBox组件可以按照指定适配规则BoxFit让子组件在父组件和FittedBox中显示


              class FittedBoxTest extends StatelessWidget {
                Widget build(BuildContext context){
                  return Scaffold(
                    appBar: AppBar(
                      title: Text('fittedBoxtest'),
                    ),
                    body: Container(
                      color: Colors.blue,
                      child: Column(
                        children: [
                          Text('index',style: TextStyle(color: Colors.red)),
                          FittedBoxEle(boxFit: BoxFit.none),
                          Text('linde',style:TextStyle(color: Colors.black)),
                          FittedBoxEle(boxFit: BoxFit.contain),//1.特别注意如果该组件时该类内作用域方法返回,FittedBoxEle布局以子元素宽高为准(并没有出现教程中同级子元素被覆盖情况)
                                                              //2.若是该类外另外定义的类得到的widget,FittedBoxEle布局宽高一外层元素宽高为准(boxFit 属性也没生效)
                          Container(
                            width: 100.0,
                            height: 100.0,
                            color: Colors.yellow,
                          )
                        ],
                      ),
                    ),
                  );
                  
                }
                Widget getFitBox(BoxFit boxFit){
                  return Container(
                    color: Colors.yellow,
                    width: 50.0,
                    height: 50.0,
                    child: FittedBox(
                      fit: boxFit,
                      child: Container(
                        color: Colors.green,
                        width: 100.0,
                        height: 100.0
                      ),
                    ),
                  );
                }
              }
              //fittedBox组件
              class FittedBoxEle extends StatelessWidget {
                final BoxFit boxFit;
                FittedBoxEle({Key key,@required this.boxFit}):super(key:key);
                @override
                Widget build(BuildContext context){
                  return Container(
                    color: Colors.yellow,
                    width: 50.0,
                    height: 50.0,
                    child: FittedBox(
                      fit: boxFit,
                      child: Container(
                        color: Colors.green,
                        width: 100.0,
                        height: 100.0
                      ),
                    ),
                  );
                }
              }

      //5.6.2 实例:单行缩放布局
        //作用:1.某行数据文字显示自动适配缩放布局

        //使用情景:1.某行数据超出row最大宽度溢出,此时需要自动进行缩放 2.当row父组件给row的约束为double.infinity(无穷大时),row的宽度由子元素总宽决定,当子元素宽度较小是就会导致缩在一起(没有均衡分布-屏幕宽度均分)

        class SingleRowScaleDemo extends StatelessWidget {
          Widget build(BuildContext context){
            return Scaffold(
              appBar: AppBar(
                title: Text('单行缩放'),
              ),
              body: Container(
                child: Column(
                  children: [
                    wRow(Text('99999989877777777777777777')),
                    wRow(Text('777')),
                    addConstraintsRule(wRow(Text('99999912222277777777777777777'))),
                    addConstraintsRule(wRow(Text('7761')))
                  ],
                ),
              ),
            );
          }
          //复制多个指标
          Widget wRow(Widget child1){
            Widget child = child1;
            return Row(
              mainAxisAlignment:MainAxisAlignment.spaceEvenly,
              children: [child,child,child],
            );
          }
          Widget addConstraintsRule(Widget child1){
            return LayoutBuilder(builder: (_,constrants){
              return FittedBox(//约束无线大
                child: ConstrainedBox(
                  constraints: constrants.copyWith(
                    minWidth: constrants.maxWidth,//约束最小值为屏幕宽度避免子组件没有占满屏幕宽度
                    maxWidth: double.infinity
                  ),
                  child: child1,
                ),
              );
            });
          }
        }

    //5.7 页面骨架

      //注意Flutter Gallery是官方提供的demo,源码位于flutter源码的examples目录下
      //一个完整的路由页面包括导航栏,抽屉菜单Drawer,底部导航等等

      class ScaffoldDemo extends StatefulWidget {
        @override
        _ScaffoldDemo createState()=>_ScaffoldDemo();
      }
      class _ScaffoldDemo extends State<ScaffoldDemo>{
        int _selectInde = 1;
        @override
        Widget build(BuildContext context){
          return Scaffold(
            appBar: AppBar(
              title: Text('scaffold page'),
              actions: [//导航栏菜单
                IconButton(icon: Icon(Icons.share), onPressed: (){
                  print(_selectInde);
                })
              ],
            ),
            body: Center(
              child: Text('$_selectInde'),
            ),
            drawer:Container(
              color: Colors.green,
            ),
            bottomNavigationBar: BottomNavigationBar(
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(icon: Icon(Icons.car_rental),title: Text('car')),
                BottomNavigationBarItem(icon: Icon(Icons.home),title: Text('home')),
                BottomNavigationBarItem(icon: Icon(Icons.search),title: Text("search")),
              ],
              currentIndex: _selectInde,
              onTap: (int index){
                setState(() {
                  _selectInde = index;
                });
              },
              fixedColor: Colors.blue,
            ),
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: (){
                print('float');
              },
            ),
          );
        }
      }

      //5.7.1 AppBar
        // AppBar({
        //   Key key,
        //   this.leading,//导航栏左边widget,常见为抽屉或者返回按钮
        //   this.automaticallyImplyLeading=true,//当leading为空时是否自动默认leading
        //   this.title,//导航栏标题
        //   this.action,//导航栏右边widget
        //   this.bottom,//导航栏底部菜单,通常为Tab按钮组
        //   this.elevation=4.0,//导航栏阴影
        //   this.backgroundColor,//背景...
        // })
        
        //当给scaffold添加了菜单抽屉是,leading自动为菜单图标按钮如果想自定义图标可以使用Builder组件
        class ScaffoldAppBarDemo extends StatelessWidget {
          @override
          Widget build(BuildContext context){
            return Scaffold(
              appBar: AppBar(
                title: Text('scaffold test'),
                leading: Builder(
                  builder: (context){
                    return IconButton(icon: Icon(Icons.backpack), onPressed: (){
                      Scaffold.of(context).openDrawer();
                    });
                  },
                ),
                actions: [
                  IconButton(icon: Icon(Icons.search), onPressed: null)
                ],
                centerTitle: true,
                backgroundColor: Colors.red,
                elevation: 10.0,
              ),
              drawer: Container(
                color: Colors.red,
              ),
            );
          }
        }

      //5.7.2 drawer自右往左滑关闭 endDrawer自左往右滑关闭

  //6可滚动组件

    //6.1.1 Sliver组件
      //之前说过布局模型有2中:1.盒模型布局 2.sliver模型布局(列表等有多个子组件时,只有出现在视口里才加载,减少消耗)

      //flutter可滚动组件有三个角色组成:1.scrollable-处理滑动手势,当滑动偏移变化构建viewport
                                    //2.viewport-视口,即可视区域,
                                    //3.sliver-列表元素

      //具体构建过程:
        //1.scrollabel根据滑动偏移位置构建viewport
        //2.viewport将视口信息和配置通过sliverConstrains传递给sliver
        //3.sliver对子组件进行构建和布局,然后确定各子组件的位置,绘制信息然后保存在geometry中

        //注意:1.scrollable为viewport父,viewport为sliver父,ListView只有一个sliver
            //2.sliver还有一个预渲染区域cacheExtent

      //6.1.2 scrollabel属性
        // Scrollable({
        //   this.axisDirection= AxisDirection.down,//滚动方向
        //   this.physics,//手势操作的响应(比如滑到底是的响应效果)
        //                 //ScrollPhysics子类:1.clampingScrollPhysics-列表滑到边界不可继续滑动-通常在android中使用
        //                                   //2.GlowingOverIndicator-实现微光效果
        //                                   //3.BouncingScrollPhysics-ios下弹效果
        //   this.controller,//接受一个ScrollController对象,默认PrimaryScrollController
        //   this.viewportBuilder,//视口构建,scrollable会调用其构建viewport,同事传递viewportOffset类型的offset参数,该参数描述显示哪部分内容;viewport本身也是widget,offset变化时并不是重新布局和绘制widget,只是更新widget需要更新部分,所以offset变化构建新的viewport并不所高消耗
        // })

      //6.1.3 viewport用于渲染当前视口中需要显示的sliver

        // Viewport({
        //   Key key,
        //   this.axiosDirection,
        //   this.crossDirection,
        //   this.anchor,
        //   require ViewportOffset offset,//滚动偏移
        //   this.cacheExtent,//预渲染区域
        //   this.cacheExtentStyle=CacheExtentStyle.pixel,//当取值pixel,预渲染区域为具体的长度,当取viewport时为当前viewport整数倍(即分为多个当前页)
        // })

      //6.1.4 sliver
        //sliver主要用于列表按需加载,只有进入预渲染区域才会进行构建布局,然后再渲染
        //sliver对应的渲染对象为RenderSliver,RenderSliver和RenderBox都继承于RenderObject,
          //不同点是RenderBox约束信息来自constrains(只有包含最大宽度-暂未理解),而RenderSliver约束信息来自SliverConstrains(具体后面会介绍)
      
      //6.1.5 可滑动组件通用属性 
        //scrollDirection,reverse(按照阅读方向的相反方向滑动:例如Axis.horizontal,如为中文阅读习惯从左到有,reverse:true时,从右往左;----阿拉伯从右往左)
        //physics,controll,cacheExtent,会穿透给Scrollable,viewport

      //6.1.6 ScrollController-控制滑动组件的滑动-后面详细介绍

      //6.1.7子节点缓存-使用情景:某个页面使用ListView实现,顶部第一个元素a需要一个接口请求获取数据渲染,当往下滑动a不在viewport区域会被销毁,重新滑动回到a区域,a又会进行请求(如此多次会高耗),为了解决这个问题使用子节点缓存-后面详细介绍

      //6.1.8 ScrollBar:1.ScrollBar安卓Material风格滚动条组件 2.CupertinoScrollBar ios风格 3.如果ios平台使用CupertinoScrollBar会自动切换成scrollBar
        //任何组件需要滚动条,讲该组件作为上述2个组件子类即可

    //6.2 singleChildScrollView

      //6.2.1 简介
        // SingleChildScrollView({
        //   this.scrollDirection = Axis.vertical,//滚动方向,默认向下滚动
        //   this.reverse=false,
        //   this.padding,
        //   bool primary,
        //   this.controller,
        //   this.child
        //   this.physics
        // })

        //primary属性,当我们没有指定controller是,primary为true,默认controller为primaryScrollController
        //注意singleChildScroll使用于超出屏幕不太多的场景,因为singleChildScroll不太支持sliver延迟加载模型,会使加载消耗很高(此时应该使用ListView)
      
      //6.2.2实例

        class SingleChildScrollDemo extends StatelessWidget {

          @override
          Widget build(BuildContext context){
            String mystr = 'ABCDEFGHIJKLMNOPQRSTXYZ';
            return Scaffold(
              appBar: AppBar(
                title: Text('singleChildScroll'),
              ),
              body: Scrollbar(
                child: SingleChildScrollView(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: mystr.split('').map((item){
                      return Container(
                        margin: const EdgeInsets.only(top:10.0,bottom:10.0),
                        color: Colors.green,
                        alignment: Alignment.center,
                        child: Text(item),
                      );
                    }).toList(),
                  ),
                ),
              ),
            );
          }
        }

      //6.3 ListView

        //6.3.1默认构造函数
          // ListView({
          //   //可滚动组件公共参数
          //   Axis scrollDirection = Axis.vertical,
          //   bool reverse = false,
          //   ScrollController controller,
          //   bool primatry,
          //   ScrollPhysics physics,
          //   EdgeInsetsGeometry padding,
          //   //...构造函数公共参数
          //   double itemExtent,//1.在滚动方向上,子组件的长度(每个子组件的长度) 2.作用,指定子组件长度滚动系统知道,减少消耗
          //   Widget prototypeItem,//使用情景:知道每一项长度想通过但不知道具体多少;列表项原型,和itemExtend类似,提高性能
          //   bool shrinkWrap,//是否将子组件总长度来设置ListView长度,默认false;通知Listview在无边界(滚动组件时需要为true)
          //   bool addAutomaticKeepAlives,//PageView组件中再介绍
          //   bool addRepaintBoundaris,//是否将子组件包含在repaintBoundary组件中,可以将其理解为一个'绘制边界',避免不必要的重绘,当子组件内容比较简单例如:色块,文字等时,没必要包着
          //   List<Widget> children,
          // })

        //6.3.2 Listview.builder() -适用于列表项较多或者列表不确定的情况

          // ListView.builder({
          //   //...
          //    require IndexedWidgetBuilder itemBuilder,//列表项构造器,滚动到具体的位置会调用该构造器
          //    int itemCount,//列表数量,为null时指无限列表
          // })

          class ListViewDemoTest1 extends StatelessWidget {
            @override
            Widget build(BuildContext context){
              return Scaffold(
                appBar: AppBar(
                  title: Text('listViewDemo '),
                ),
                body: Container(
                  child: ListView.builder(
                      itemCount: 50,
                      itemExtent: 20,
                      itemBuilder: (BuildContext context,int index){
                        return ListTile(title: Text('下标$index'),);
                      },
                    
                  ),
                ),
              );
            }
          }

        //6.3.3 ListView.separated -可以在生成列表项之间生成分割组件
                  //比.builder多了一个separatorBuilder参数,该参数是分割组件生成器

          class ListViewSeparatedTest1 extends StatelessWidget {
            @override
            Widget build(BuildContext context){
              Divider divider1 = Divider(color:Colors.red,);
              Divider divider2 = Divider(color:Colors.blue,);
              return Scaffold(
                appBar: AppBar(
                  title: Text('separated'),
                ),
                body: Container(
                  child: ListView.separated(
                    itemCount: 50,
                    itemBuilder: (BuildContext context,int index){
                      return Text('序号:$index');
                    },
                    separatorBuilder: (BuildContext context,int index){
                      return index%2==0?divider2:divider1;
                    },
                  ),
                ),
              );
            }
          }

        //6.3.4 固定列表高度
          //当我们知道列表高度都相同强烈建议使用itemExtent或者prototypeItem -LayoutLogPrint(自定义打印盒子宽高)

          class ListViewSureHeightTest extends StatelessWidget {
            @override
            Widget build(BuildContext context){
              return Scaffold(
                appBar: AppBar(
                  title: Text('sure height'),
                ),
                body: Container(
                  child: Container(
                    child: ListView.builder(
                      itemCount: 50,
                      // prototypeItem:ListTile(title: Text('1'),),//使用不了
                      itemExtent: 50,
                      itemBuilder: (BuildContext context,int index){
                        return ListTile(title:Text('序号:$index'),);
                      },
                    ),
                  ),
                ),
              );
            }
          }

        //6.3.5 ViewList 原理
          //ViewList包含了scrollable,viewport,sliver
          //1.viewList中子列表项是RenderBox,不是sliver
          //2.viewList中只有一个sliver,按需加载功能就是在sliver中实现的
          //3.viewList的sliver默认是sliverList,如果是指定itemExtent则会使用sliverFixedExtentList
              //如果prototypitem不为空,则会使用sliverPrototypeExtentList

        //6.3.6 无限加载列表-应用场景:当列表数据分批次请求加载
          
          class InfinityLoadingListTest extends StatefulWidget {
            @override
            _InfinityLoadingListTest createState()=> _InfinityLoadingListTest();
          }
          class _InfinityLoadingListTest extends State<InfinityLoadingListTest>{
            static String _loadingStr = '##loading##';
            List<String> _wordList = [_loadingStr];
            @override
            void initState(){
              _getDataList();
              super.initState();
            }
            @override
            Widget build(BuildContext context){
              return Scaffold(
                appBar: AppBar(
                  title: Text('infinify loading list'),
                ),
                body: Container(
                  child: ListView.separated(
                    itemCount: _wordList.length,
                    separatorBuilder: (BuildContext context,int index){
                      return Divider(color: Colors.green,height: 2,);
                    },
                    //特别注意好像是预渲染的原因,初始化时会自动加载2次即初始化时会由于40条数据
                    itemBuilder: (BuildContext context,int index){
                       if(_wordList[index]==_loadingStr){//当按需加载到最后一个时
                          //最大加载100条
                          
                          if(_wordList.length-1<100){
                            print('懒加载:$_wordList');
                            _getDataList();
                            return Container(
                              padding:const EdgeInsets.all(16.0),
                              alignment:Alignment.center,
                              child:SizedBox(
                                width:24.0,
                                height:24.0,
                                child:CircularProgressIndicator(strokeWidth:2.0)
                              )
                            );
                          }else{
                            return Container(
                              padding: const EdgeInsets.all(16.0),
                              alignment: Alignment.center,
                              child: Text(
                                '没有更多了',
                                style:TextStyle(
                                  color:Colors.grey
                                )
                              ),
                            );
                          }
                       }
                       return ListTile(title: Text(_wordList[index]));
                    },
                  ),
                ),
              );
            }
            //模仿分批次加载数据,每次随机生成20个单词
            void _getDataList(){
              print('进入加载');
              Future.delayed(Duration(seconds:5)).then((item){
                List<String> dataList = List<String>();
                for(int index=_wordList.length;index<(20+_wordList.length);index++){
                  dataList.add('序号:$index');
                }
                setState(() {
                  _wordList.insertAll(_wordList.length-1, dataList);
                  print('加载完毕');
                  print(_wordList);
                });
              });
            }
          }

          //有一种情景:在头部和尾部需要加标题和结束提示

          class InfinityLoadingListTest1 extends StatefulWidget{
            _InfinityLoadingListTest1 createState()=>_InfinityLoadingListTest1();
          }
          class _InfinityLoadingListTest1 extends State<InfinityLoadingListTest1>{
            static String _loadingStr = '##loading##';
            List<String> _wordList = [_loadingStr];
            @override
            void initState(){
              _getDataList();
              super.initState();
            }
            @override
            Widget build(BuildContext context){
              return Scaffold(
                appBar: AppBar(
                  title: Text('head after part'),
                ),
                body: Container(
                  child: Column(
                    children: [
                      Container(
                        width: 100.0,
                        height: 100.0,
                        color: Colors.green,
                        child: Text('头部'),
                      ),
                      Expanded(
                        child: ListView.separated(
                          itemCount: _wordList.length,
                          separatorBuilder: (BuildContext context,int index){
                            return Divider(color: Colors.green,height: 2,);
                          },
                          //特别注意好像是预渲染的原因,初始化时会自动加载2次即初始化时会由于40条数据
                          itemBuilder: (BuildContext context,int index){
                            if(_wordList[index]==_loadingStr){//当按需加载到最后一个时
                                //最大加载100条
                                
                                if(_wordList.length-1<100){
                                  print('懒加载:$_wordList');
                                  _getDataList();
                                  return Container(
                                    padding:const EdgeInsets.all(16.0),
                                    alignment:Alignment.center,
                                    child:SizedBox(
                                      width:24.0,
                                      height:24.0,
                                      child:CircularProgressIndicator(strokeWidth:2.0)
                                    )
                                  );
                                }else{
                                  return Container(
                                    padding: const EdgeInsets.all(16.0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      '没有更多了',
                                      style:TextStyle(
                                        color:Colors.grey
                                      )
                                    ),
                                  );
                                }
                            }
                            return ListTile(title: Text(_wordList[index]));
                        },
                  ),
                      ),
                      Container(
                        width: 100.0,
                        height: 100.0,
                        color: Colors.blue,
                        child:Text('尾部'),
                      )
                    ],
                  ),
                ),
              );
            }
             //模仿分批次加载数据,每次随机生成20个单词
            void _getDataList(){
              print('进入加载');
              Future.delayed(Duration(seconds:5)).then((item){
                List<String> dataList = List<String>();
                for(int index=_wordList.length;index<(20+_wordList.length);index++){
                  dataList.add('序号:$index');
                }
                setState(() {
                  _wordList.insertAll(_wordList.length-1, dataList);
                  print('加载完毕');
                  print(_wordList);
                });
              });
            }
          }
          
        //6.3.7 ListView.custom方法，它需要实现一个SliverChildDelegate---后续有空再深入学习

      //6.4滚动监听及控制
        //本节主要内容:1.学习ScrollController 2.路由切换时如何保存滚动距离

        //6.4.1 ScrollController
          // ScrollController({
          //   double initialScrollOffset=0.0,//初始滚动w位置
          //   bool keepScrollOffset=true,//是否保存滚动条位置
          // })

          //ScrollController实例的一些属性和方法
            //1. .offset -当前滚动组件的滚动位置
            //2. jumpTo(double offset) 和animateTo(double offset,...),这2个方法都是跳转到指定位置,区别是后一个有动画,其他属性后面再学习

          //1.滚动监听 ScrollController _controller _controller.addListener(()=>print(_controller.offset))  0627 0170 0328 7923

          //需求:当滑动组件滑动超过1000时,显示回到顶部按钮

          class BackTopDemo extends StatefulWidget {
            @override
            _BackTopDemo createState()=>_BackTopDemo();
          }
          class _BackTopDemo extends State<BackTopDemo> {
            //滚动条控制器
            ScrollController _controller = ScrollController();
            //是否显示回到顶部按钮
            bool _isGoTop = false;
            @override
            void initState(){
              print('initstate');
              _controller.addListener(() {
                  print('offse:${_controller.offset}');
                  if(_controller.offset>700){
                    print('offse2:${_controller.offset}');
                    setState(() {
                      _isGoTop = true;
                    });
                  }else{
                   setState(() {
                      _isGoTop = false;
                   });
                  }
              });
              super.initState();
            }
            //避免内存泄漏
            @override
            void dispose(){
              _controller.dispose();
              super.dispose();
            }
            @override
            Widget build(BuildContext context){
              return Scaffold(
                appBar: AppBar(
                  title: Text('测试'),
                ),
                body:Scrollbar(
                  child: ListView.separated(
                    itemCount: 100,
                    itemBuilder: (BuildContext context,int index){
                      return ListTile(title: Text('序号:$index'));
                    },
                    controller: _controller,
                    separatorBuilder: (BuildContext context,int index){
                      return Divider(color: Colors.green);
                    },
                  ),
                ),
                floatingActionButton: _isGoTop?FloatingActionButton(
                  child: Icon(Icons.arrow_upward),
                  onPressed: (){
                    _controller.animateTo(.0, duration: Duration(microseconds: 300), curve: Curves.ease);
                  },
                ):null,
              );
            }
          }


          //一.滚动位置恢复
            //PageStorage 1.保存路由页面相关组件数据,并不影响ui(是个功能组件)
                        //2.简而言之是个存储同,将子树中widget数据通过PageStorageKey存储各自的数据
                        //3.当ScrollController.keepScrollOffset为false,每次重新创建加载时调用的是initScrollerOffset;为true如果之前有存储为offset使用存储的,第一次创建时使用initScrollerOffset
                        //4.通常使用于路由页面切换或者tab切换时tab没有保存

          //二.ScrollPosition
            //1.ScrollPosition用来保存滚动组件的位置,一个ScrollController可以被多个滚动组件使用
            //2.offset只是key position才是value double get offset = position.pixels;
            //3.当ScrollController被多个组件同时使用时可以使用 _controller.positions.elementAt(0).pixels获取
            //4.ScrollController 方法:1._controller.jumpTo() 2._controller.animateTo(AlignTestDemo)

          //三.ScrollController控制原理
            //1.当ScrollController和可滚动组件关联后,ScrollController调用createScrollPosition()生成ScrollPosition;
              //然后调用attach将ScrollPosition添加到ScrollController的position中;这一步称为注册位置,然后才可以调用animateTo,jumpTo
            //2.当可滚动组件被销毁后,ScrollController调用detach方法将scrollPosition从ScrollController的position中移除
            //3.ScrollController调用animateTo和jumpTo时会调用内部所有ScrollController的animateTo和jumpTo

        //6.4.2 滚动监听
          //1.滚动通知
            //(1)Widget树中widget可以通过Notification(通知)向父级(或者祖先)发送消息,上级通过NotificationListener接收消息(类似web中冒泡)
            //(2)ScrollBar通过scrollNotification通知
            //(3)NotificationListener和_controller.addListener差异:NotificationListener除了得到当前滚动位置还有viewport一些信息

          //2.实例
            class NotificationProgressDemo extends StatefulWidget {
              @override
              _NotificationProgressDemo createState()=>_NotificationProgressDemo();

            }
            class _NotificationProgressDemo extends State<NotificationProgressDemo>{
              String _progressStr = '0%';
              @override
              Widget build(BuildContext context){
                return Scaffold(
                  appBar: AppBar(
                    title: Text('notification listener demo'),
                  ),
                  body: Scrollbar(
                    child: NotificationListener<ScrollNotification>(
                      onNotification: (ScrollNotification notification){
                        double _progress = notification.metrics.pixels / notification.metrics.maxScrollExtent*100;
                        setState(() {
                          _progressStr = '${_progress.toStringAsFixed(2)}%';
                        });
                        //extentBefore:视口顶部距离初始点的距离 extentInside:视口长度 extentAfter:视口底部距离剩余没有展示底部的距离
                        String mystr = 'top:${notification.metrics.extentBefore};bottom:${notification.metrics.extentAfter};viewport:${notification.metrics.extentInside};是否滑到了边界:${notification.metrics.atEdge}';
                        print(mystr);
                        return false;
                        // return true;//放开这个滚动条失效
                      },
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          ListView.separated(
                            itemCount: 100,
                            itemBuilder: (BuildContext context,int index){
                              return ListTile(title: Text('序号:$index'));
                            },
                            separatorBuilder: (BuildContext context,int index){
                              return Divider(color: Colors.green);
                            }
                          ),
                          CircleAvatar(
                            radius: 30.0,
                            child: Text(_progressStr),
                            backgroundColor: Colors.black54,
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }
            }

      //6.5 AnimatedList
        //AnimatedList 和 ListView类似,只是插入元素时会有动画
        //方法:1.void itemInsert(int index,{Duration duration=_kDuration})
        //     2.void removeItem(int index,AnimatedListRemoveItemBuilder builder,{Duration duration=_kDuration})

          class AnimatedListAddDeleTest extends StatefulWidget{
            @override
            _AnimatedListAddDeleTest createState()=>_AnimatedListAddDeleTest();
          }
          class _AnimatedListAddDeleTest extends State<AnimatedListAddDeleTest>{
            
            final GlobalKey<AnimatedListState> globalKey = GlobalKey<AnimatedListState>();
            //初始数据
            List<int> _data= List<int>();
            @override
            void initState(){
              super.initState();
              for(int index=0;index<5;index++){
                _data.add(index);
              }
            }
            @override
            Widget build(BuildContext context){
              return Scaffold(
                appBar: AppBar(
                  title: Text('animatedListAddDeletest'),
                ),
                body: Stack(
                  children: <Widget>[
                    AnimatedList(
                      key: globalKey,
                      initialItemCount: _data.length,
                      itemBuilder: (BuildContext context,int index,Animation animation){
                        return FadeTransition(
                          opacity: animation,
                          child: _buildChild(index,context),
                        );
                      },
                    ),
                    Positioned(
                      bottom: 50.0,
                      left:0.0,
                      right: 0.0,
                      child: FloatingActionButton(
                        child: Icon(Icons.add),
                        onPressed: (){
                          _data.add(_data.length);
                          setState(() {
                            //告诉列表有新增的列表项
                            globalKey.currentState.insertItem(_data.length-1);
                            print('length:${_data.length}');
                          });
                        },
                      ),
                    )
                  
                  ],
                ),
              );
            }
            Widget _buildChild(int index,BuildContext context){
             String _title = '序号:$index';
              return  ListTile(
                title: Text(_title),
                trailing: IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: (){
                     globalKey.currentState.removeItem(index,(context,animation){
                        Widget item = _buildChild(index, context);
                        _data.removeAt(index);
                        // 删除动画是一个合成动画：渐隐 + 缩小列表项告诉
                        return FadeTransition(
                          opacity: CurvedAnimation(
                            parent: animation,
                            curve: const Interval(.5,1.0)
                          ),
                          // opacity: animation,
                          child: SizeTransition(//让列表项高度逐渐减小
                            child: item,
                            sizeFactor: animation,
                            axisAlignment: 0.0,
                          ),
                        );
                     },duration: Duration(milliseconds: 100));

                  },
                ),
              );
            }
          }

      //6.6 GridView

        //6.6.1默认构造函数
          // GridView({
          //   Key key,
          //   Axis scrollDirection= Axis.vertical,
          //   bool reverse = false,
          //   ScrollController conntroller,
          //   bool primary,
          //   ScrollPhysics physics,
          //   bool shrinkWrap = false,
          //   EdgeInsetsGeometry padding,
          //   @require this.gridDelegate,
          //   bool addAutomaticKeepAlives = true,
          //   bool addRepaintBoundaries = true,
          //   double catchExtent,
          //   List<Widget> children = const <Widget>[]
          // })

          //GridView很多属性和ListView一样的,gridDelegate不同
          //gridDelegate类型 SliverGridViewDelegate-子类:1.SliverGridViewWithFixedCrossAxisCount 2.SliverGridViewDelegateWithMaxCrossAxisExtent

            // SliverGridViewWithFixedCrossAxisCount({
            //   //该子类实现了一个横轴为固定数量的layout算法
            //   @required double crossAxisCount,//横轴的元素数量,数量确定也确定了每个元素的长度 viewport/cout = width
            //   double mainAxisSpacing=0.0,//主轴方向的间距
            //   double crossAxisSpacing=0.0,//横轴的间距
            //   double childAspectRatio=1.0//子元素在横轴和主轴上长度比例,可以发现子元素的长度与这个参数有关
            // })

            // SliverGridViewWithMaxCrossAxisExtent({
            //   //该子类实现了在横轴上每个子元素最大长度的layout算法
            //   double maxCrossAxisExtent,//横轴每个子元素最大的长度(每个子元素长度一样,超出vieport换行)
            //   double mainAxisSpacing,
            //   double crossAxisSpacing,
            //   double childAspectRatio,//同上一个子类用法相同
            // })

        //6.6.2 GridView.count-内部使用SliverGridViewWithFixedCrossAxisCount构造函数用法和该类一致效果也类似
        //6.6.3 GridView.extent-内部使用SliverGridViewWithMaxCrossAxisExtent构造函数用法和该类一致效果也类似
        //6.6.4 GridView.builder-上述方法用于子元素较少情况,当子元素个数很多时需要适用builder
          // GridView({
          //   //...
          //   @required SliverGridDelegate gridDelegate,
          //   @required IndexedWidgetBuilder itemBuilder,
          // })

          //示例

            class GridViewBuilderTest extends StatefulWidget {

              _GridViewBuilderTest createState()=>_GridViewBuilderTest();
            } 
            class _GridViewBuilderTest extends State<GridViewBuilderTest>{
              List<IconData> _iconList = List<IconData>();
              @override
              void initState(){
                super.initState();
                _delayedGetData();
              }
              @override
              Widget build(BuildContext context){
                return Scaffold(
                  appBar: AppBar(
                    title: Text('gridvieew builder'),
                  ),
                  body: GridView.builder(
                    itemCount: _iconList.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisSpacing: 20.0,
                      childAspectRatio: 1.0
                    ),
                    itemBuilder: (context,index){
                      if(index==_iconList.length-1&&_iconList.length<20){
                        _delayedGetData();
                      }
                      return Icon(_iconList[index]);
                    },
                  ),
                );
              }
              void _delayedGetData(){
                Future.delayed(Duration(milliseconds: 800)).then((e){
                  setState(() {
                    _iconList.addAll([
                      Icons.face,
                      Icons.search,
                      Icons.east,
                      Icons.hail
                    ]);
                  });
                });
              }
            }

      //6.7 PageView与页面缓存

        //6.7.1PageView
          //应用场景:1.页面切换tab布局 2.图片轮播 3.抖音上下滑等等

          // PageView({
          //   Key key,
          //   this.scrollDirection = Axis.vertical,
          //   this.reverse = false,
          //   PageController controller,
          //   this.physics,
          //   List<Widget> children = const <Widget>[],
          //   this.onPageChange,//为true时每次滑动强制切换页面,false则反
          //   this.pageSnaping=true,//辅助功能后面解释
          //   this.allowImplicitScrolling,//
          //   this.padEnds=true,
          // })

          class PageProductTest1 extends StatefulWidget {
            final String title;
            PageProductTest1({Key key,@required this.title}):super(key:key);
            @override
            _PageProductTest1 createState()=>_PageProductTest1();
          }
          class _PageProductTest1 extends State<PageProductTest1>{

            @override
            Widget build(BuildContext context){
              print('title:${widget.title}');
              return Center(
                child: Text(widget.title,style:TextStyle(color: Colors.green,fontSize: 40.0))
              );
            }
          }
          //滑动页面
          class PageListTest1 extends StatefulWidget {

            @override
            _PageListTest1 createState()=>_PageListTest1();
          }
          class _PageListTest1 extends State<PageListTest1>{
            List<Widget> _list = List<Widget>();
            @override
            void initState(){
              super.initState();
              for(int index=0;index<5;index++){
                // print('build:$index');
                _list.add(PageProductTest1(title: '序号:${index+1}'));
              }
            }
            @override
            Widget build(BuildContext context){
              return Scaffold(
                appBar: AppBar(
                  title: Text('pagelist test'),
                ),
                body: Container(
                  color: Colors.black54,
                  child: PageView(
                    children: _list,
                    // scrollDirection: Axis.vertical,
                  ),
                ),
              );
            }
          }
        
        //6.7.2 页面缓存-通过上述例子我们可以发现每次切换页面时子widget是重新加载的,这在实际应用中会消耗很高,这时候就需要缓存
          //allowImplicitScrolling 可缓存前后一页但是并不满足需求 --正统方法看下一章

        //6.8可滚动组件子项缓存
          //1.viewport(可视区)和cacheExtent(预渲染区)称为加载区
          //2.当RenderObject的keepAlive为false时,划动加载区,那么不在加载区组件会被销毁;true时会缓存加载过的组件
          //3.keepAlive状态由AutomaticKeepAlive改变;需要AutomaticKeepAlive的client(功能组件AutomaticKeepAliveClientMixin)通过keepAliveNotification去通知AutomaticKeepAlive

            //示例:
             class PageProductTest2 extends StatefulWidget {
                final String title;
                PageProductTest2({Key key,@required this.title}):super(key:key);
                @override
                _PageProductTest2 createState()=>_PageProductTest2();
              }
              class _PageProductTest2 extends State<PageProductTest2> with AutomaticKeepAliveClientMixin{

                @override
                Widget build(BuildContext context){
                  print('title:${widget.title}');
                  super.build(context);//AutomaticKeepAliveClientMixin内部将wantKeepAlive通过keepAliveNotification传递给AutomaticKeepAlive
                  return Center(
                    child: Text(widget.title,style:TextStyle(color: Colors.green,fontSize: 40.0))
                  );
                }
                @override
                bool get wantKeepAlive =>true;
              }
            class KeepAliveCache extends StatefulWidget {
              @override
              _KeepAliveCache createState()=>_KeepAliveCache();
            }
            class _KeepAliveCache extends State<KeepAliveCache> {
              List<Widget> _list = List<Widget>();
              @override
              void initState(){
                super.initState();
                for(int index=0;index<5;index++){
                  _list.add(PageProductTest2(title:'内容:$index'));
                }
              }
              @override
              Widget build(BuildContext context){
                return Scaffold(
                  appBar: AppBar(
                    title: Text('automatic keep alive client minxin'),
                  ),
                  body: Container(
                    color: Colors.purple,
                    child: PageView(
                      scrollDirection: Axis.horizontal,
                      children: _list,
                    ),
                  ),
                );
              }
            }
            //需要注意，如果我们采用 PageView.custom 构建页面时没有给列表项包装 AutomaticKeepAlive 父组件，

          //6.8.2 KeepAliveWrapper-虽然上述方法通过混入可以实现每个列表项缓存,但是不够优雅,这里可以将其封装成一个组件方便使用

            class KeepAliveWrapper extends StatefulWidget {
              final bool keepAlive;
              final Widget child;
              KeepAliveWrapper({Key key,this.keepAlive=false,@required this.child}):super(key:key);
              @override
              _KeepAliveWrapper createState()=>_KeepAliveWrapper();
            }
            class _KeepAliveWrapper extends State<KeepAliveWrapper> with AutomaticKeepAliveClientMixin{

              @override
              Widget build(BuildContext context){
                super.build(context);
                return widget.child;
              }
              //当父widget重建时会调用
              @override
              void didUpdateWidget(covariant KeepAliveWrapper oldWidget){
                if(oldWidget.keepAlive != widget.keepAlive){
                  //AutomaticKeepAliveClientMixin内部调用
                  updateKeepAlive();
                }
                super.didUpdateWidget(oldWidget);
              }
              //状态
              @override
              bool get wantKeepAlive =>  widget.keepAlive;
            }
            class ListItem extends StatefulWidget {
              const ListItem({Key key, @required this.index}) : super(key: key);
              final int index;

              @override
              _ListItemState createState() => _ListItemState();
            }

            class _ListItemState extends State<ListItem> {
              @override
              Widget build(BuildContext context) {
                return ListTile(title: Text('序号${widget.index}'));
              }

              @override
              void dispose() {
                print('dispose ${widget.index}');
                super.dispose();
              }
            }
            class TestKeepAliveWrapper extends StatefulWidget {

              @override
              _TestKeepAliveWrapper createState()=>_TestKeepAliveWrapper();
            }
            class _TestKeepAliveWrapper extends State<TestKeepAliveWrapper>{
              @override
              Widget build(BuildContext context){
                return Scaffold(
                  appBar: AppBar(
                    title: Text(' custorm keep alive wrapper'),
                  ),
                  body: Container(
                    color: Colors.green,
                    child: ListView.separated(
                      itemCount: 30,
                      itemBuilder: (context,index){
                        // print('序号:$index');
                        // return ListTile(title: Text('序号:$index'));
                        bool _flag = false;
                        if(index%2==0){
                          _flag = true;
                        }
                        print('序号:$index;flag:$_flag');
                        return KeepAliveWrapper(
                          keepAlive: _flag,
                          child: ListTile(title: Text('序号:$index'))
                        );
                      },
                      separatorBuilder: (context,index){
                        return Divider(color: Colors.black,);
                      },
                    ),
                  ),
                );
              }
              @override
              void dispose(){
                super.dispose();//0854 8753693
              }
            }

        //6.9 TabBarView-TabBarView是Material组件库中提供了Tab布局组件,通常和TabBar配合使用

          //6.9.1 TabBarView
            // TabBarView({
            //   Key key,
            //   @required this.children,//page页面
            //   this.controller,//TabController,//通常用于监听页面变化,如果没有指定,默认会寻找最近一个DefaultTabController
            //   this.physics,
            //   this.dragStartBehavior = DragStartBehavior
            // })

          //6.9.2 TabBar 
            //TabBar有很多参数,很多属性都是在配置indicator(高亮下面白线)和label

            // const TabBar({
            //   Key key,
            //   @require this.tabs,//具体的tab需要我们创建
            //   this.controller,
            //   this.isScrollable=false,//是否可以滑动
            //   this.padding,
            //   this.indicatorColor,//指示器颜色,默认是高度为2的一条下划线
            //   this.automaticIndicatorColorAdjustment,
            //   this.indicatorWeight,//指示器高度
            //   this.indicatorPadding,//指示器padding
            //   this.indicator,//指示器
            //   this.indicatorSize,//指示器长度,2个可选值,一个tab的长度,一个label的长度
            //   this.labelColor,
            //   this.labelStyle,
            //   this.labelPadding,
            //   this.unselectedLableColor,
            //   this.unselectedLableStyle,
            //   this.mouseCursor,
            //   this.onTap,
            // })

            //TabBar通常位于AppBar的底部,如果要和TabViewBar连动那么这俩需要共用一个TabController

              // const Tab({
              //   Key key,
              //   this.text,//文本
              //   this.icon,//图标
              //   this.iconMargin = const EdgeInsets.only(bottom: 10.0),
              //   this.height,
              //   this.child,//自定义widget
              //   //注意text和child是互相排斥的
              // })
          
          //6.9.3实例

            //首先要注意TabController的参数包含vsync,所以需要混入SingleTickerProviderStateMixin
            class TabViewDemoTest extends StatefulWidget{
              _TabViewDemoTest createState()=> _TabViewDemoTest();
            }
            class _TabViewDemoTest extends State<TabViewDemoTest> with SingleTickerProviderStateMixin{

              TabController _controller;
              List<String> _list = ['历史','政治','经济','人文'];
              @override
              void initState(){
                super.initState();
                _controller = TabController(length: _list.length,vsync: this);
              }
              @override
              Widget build(BuildContext context){
                return Scaffold(
                  appBar: AppBar(
                    title: Text('tab controller demo test'),
                    bottom: TabBar(
                      controller: _controller,
                      tabs:_list.map((item){
                        // return Tab(
                        //   child: TextButton.icon(
                        //     label: Text(item),
                        //     icon: Icon(Icons.tab),
                        //     onPressed: (){
                        //       print(item);
                        //     },
                        //   ),
                        // );
                        return Tab(child: Text(item));
                      }).toList()
                    ),
                  ),
                  body:TabBarView(
                    controller: _controller,
                    children: _list.map((item){
                      return KeepAliveWrapper(
                        child: Container(
                          alignment: Alignment.center,
                          child: Text(item, textScaleFactor: 5),
                        ),
                      ); 
                    }).toList()
                  )
                );
              }
              @override
              void dispose(){
                super.dispose();
                // 释放资源
                _controller.dispose();
              }
            }

            //页面缓存
            //因为TabBarView 内部封装了 PageView，如果要缓存页面，可以参考 PageView 一节中关于页面缓存的介绍。

          //6.10 CustomScrollView和Slivers

            //6.10.1 CustomScrollView-主要作用:提供一个scrollable和viewport将多个Sliver合并起来

              class CustomScrollViewDemo extends StatelessWidget {

                @override
                Widget build(BuildContext context){
                  return Scaffold(
                    appBar: AppBar(
                      title: Text('custom scroll view demo'),
                    ),
                    body: _getSpecialWidet(context),
                  );
                }
                //常规2个可滚动组件
                Widget _getNOmalWidget(BuildContext context){
                  ListView _mylist =ListView.builder(
                    itemCount: 10,
                    itemBuilder: (context,index){
                      return  ListTile(title: Text('$index'));
                    },
                  );
                  return Column(
                    children: [
                      Expanded(
                        child: _mylist,
                      ),
                      Divider(color: Colors.blue),
                      Expanded(
                        child: _mylist,
                      )
                    ],
                  );
                }
                //合并2个可滚动组件
                Widget _getSpecialWidet(BuildContext context){
                  //特别注意:如果列表项相同使用SliverFixedExtentList或者SliverprototypeExtentList,否则用SliverList
                  SliverFixedExtentList _specialList = SliverFixedExtentList(
                    itemExtent: 56,
                    delegate: SliverChildBuilderDelegate(
                      (context,index){
                        return ListTile(title: Text('序号:$index'));
                      },
                      childCount: 10
                    ),
                  );
                  return CustomScrollView(
                    slivers: [
                      _specialList,
                      _specialList
                    ],
                  );
                }
              }

            //6.10.2 Flutter 中常用的Sliver
              //Sliver名称                  功能                              对应可滚动组件
              //SliverList                  列表                              ListView
              //SliverFixedExtentList       高度固定的列表                     ListView，指定itemExtent时
              //SliverAnimatedList          删除添加列表项可以执行动画          AnimatedList
              //SliverGrid                  网格                              GridView
              //SliverPrototypeExtentList   根据原型生成高度固定的列表          ListView，指定prototypeItem 时
              //SliverFillViewport          包含多个子组件，每个都可以填满屏幕   PageView     

            //除了列表对应的Sliver外还有一些用于Sliver修饰布局的组件

            //   Sliver名称	                     对应 RenderBox
            // SliverPadding	                     Padding
            // SliverVisibility、SliverOpacity	   Visibility、Opacity
            // SliverFadeTransition	             FadeTransition
            // SliverLayoutBuilder	               LayoutBuilder

            // 还有一些其他常用的 Sliver：
            // Sliver名称	             说明
            // SliverAppBar	           对应 AppBar，主要是为了在 CustomScrollView 中使用。
            // SliverToBoxAdapter	     一个适配器，可以将 RenderBox 适配为 Sliver，后面介绍。
            // SliverPersistentHeader	滑动到顶部时可以固定住，后面介绍。
            // Sliver系列 Widget       比较多，我们不会一一介绍，读者只需记住它的特点，需要时再去查看文档即可。
            //                         上面之所以说“大多数”Sliver都和可滚动组件对应，
            //                         是由于还有一些如SliverPadding、SliverAppBar 等是和可滚动组件无关的，
            //                         它们主要是为了结合CustomScrollView一起使用，
            //                         这是因为CustomScrollView的子组件必须都是Sliver。

              //例子:
                class SliverEleDemos extends StatelessWidget {
                  @override 
                  Widget build(BuildContext context){
                    return Material(
                      child:CustomScrollView(
                        slivers: [
                          SliverAppBar(
                            // pinned: true,//滑到顶端时会固定住
                            expandedHeight: 250,
                            flexibleSpace: FlexibleSpaceBar(
                              title: Center(
                                child: Text('demo'),
                              ),
                              background: Image.asset(
                                'image/back.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SliverPadding(
                            padding: const EdgeInsets.all(20),
                            sliver: SliverGrid(
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                crossAxisSpacing: 20.0,
                                mainAxisSpacing: 10.0,
                                childAspectRatio: 2.0
                              ),
                              delegate: SliverChildBuilderDelegate(
                                (BuildContext context,int index){
                                  return Container(
                                    alignment: Alignment.center,
                                    color:Colors.cyan[100*(index % 9)],
                                    child: Text('grid of index is $index'),
                                  );
                                },
                                childCount: 20
                              ),
                            ),
                          ),
                          SliverFixedExtentList(
                            itemExtent: 30.0,
                            delegate: SliverChildBuilderDelegate(
                              (BuildContext context,int index){
                                return Container(
                                    alignment: Alignment.center,
                                    color:Colors.cyan[100*(index % 9)],
                                    child: Text('grid of index is $index'),
                                  );
                              },
                              childCount: 12
                            ),

                          )
                        ],
                      )
                    );
                  }
                }

            //SliverToBoxAdapter:1.并不是所有组件都有对应的Sliver版本 2.所以需要SliverToBoxAdapter讲RenderBox适配成Sliver 3.例如CustomScrollerView中需要一个一个横向滑动的PageView,这时候就需要适配
              class SliverToBoxAdaperDemo extends StatelessWidget {
                @override
                Widget build(BuildContext context){
                  return Material(
                    child: CustomScrollView(
                      slivers: [
                        SliverToBoxAdapter(
                          child: Container(
                            height: 200.0,
                            color: Colors.green,
                            child: PageView(
                              children: [
                                Center(
                                  child: Text('adapter test1 ')
                                ),
                                Center(
                                  child: Text('adapter test2 ')
                                ),
                                Center(
                                  child: Text('adapter test3 ')
                                ),
                              ],
                            ),
                          ),
                        ),
                        SliverPadding(
                          padding: const EdgeInsets.all(20),
                          sliver: SliverGrid(
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              crossAxisSpacing: 20.0,
                              mainAxisSpacing: 10.0,
                              childAspectRatio: 2.0
                            ),
                            delegate: SliverChildBuilderDelegate(
                              (BuildContext context,int index){
                                return Container(
                                  alignment: Alignment.center,
                                  color:Colors.cyan[100*(index % 9)],
                                  child: Text('grid of index is $index'),
                                );
                              },
                              childCount: 30
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              }
              //注意:如果 CustomScrollView 有孩子也是一个完整的可滚动组件且它们的滑动方向一致，则 CustomScrollView 不能正常工作。要解决这个问题，可以使用 NestedScrollView，这个我们将在下一节介绍。
              //(上述PageView中scrollable方向和CustomScrollView不一致)


            //SliverPersistentHeader:作用当CustomScrollView滑动到顶部时,可以将组件固定在顶部
              //注意:SliverPersistentHeader设计的初衷是为了实现SliverAppBar
              // const SliverPersistentHeader({
              //   Key key,
              //   @required SliverPersistentHeaderDelegate delegate,
              //   this.pinned = false,//header 滑动到可视区域顶部是否固定在顶部
              //   this.floating = false,//正文部分介绍
              // })

              //floating的作用是:当pinned为false时,header可以滑出可视区域---后续理解了再补充

              //delegate是用于生成header的委托,类型为SliverPersistentHeaderDelegate是一个抽象类,需要我们实现

                abstract class SliverPersistentHeaderDelegate {
                  //当pinned为true时,header的最大高度(刚刚固定到顶部时为最大高度)
                  double get maxExtent;
                  //当pinned为true时,header固定在顶部时继续往上滑,maxExtent逐渐减小到minExtent
                  double get minExtent;
                  //构建header shrinkOffset取值范围为[0,maxExtent],当header刚刚固定在顶部时shrinkOffset为0,往上滑渐增

                  //overlapsContent -一般不建议使用,使用时一定要小心,后面解释
                  Widget  build(BuildContext context,double shrinkOffset,bool overlapsContent);

                  //header是否需要重建 通常父级widget的StatefullWidget状态变化时会调用
                  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate);
                  //下面几个属性是SliverPersistentHeaderDelegate在SliverAppBar中实现floating,snap效果中会用到
                  TickerProvider get vsync => null;
                  FloatingHeaderSnapConfiguration get snapConfiguration  => null;
                  OverScrollHeaderStretchConfiguration get stretchConfiguration => null;
                  PersistentHeaderShowOnScreenConfiguration get showOnScreenConfiguration => null;

                }
                //注意:如果我们想要header高度固定则讲maxExtent和minExtent固定即可

                  //为了构建header我们必须要定义一个类,让她继承自SliverPersistentHeaderDelegate,这会增加使用成本,为此我们要封装一个构造器SliverHeaderDelegate
                   typedef SliverHeaderBuilder = Widget Function(
                    BuildContext context,//注意这里是顺序参数
                    double shrinkOffsest,
                    bool overlapsContent
                   );

                   class SliverHeaderDelegate extends SliverPersistentHeaderDelegate  {
                      final double maxHeight;
                      final double minHeight;
                      final SliverHeaderBuilder builder;
                      
                      //构造函数
                      SliverHeaderDelegate({
                        @required this.maxHeight,
                        this.minHeight=0,
                        @required Widget child
                      }) : builder = ((a,b,c)=>child),assert(minHeight <= maxHeight && minHeight>0);
                     
                      //最大最小高度相同
                      SliverHeaderDelegate.fixedHeight({
                        @required height,
                        @required Widget child
                      }) : builder = ((a,b,c)=> child),maxHeight=height,minHeight=height;

                      //需要自定义buildershi 
                      SliverHeaderDelegate.builder({
                        @required this.maxHeight,
                        this.minHeight=0,
                        @required  this.builder//这里是builder
                      });

                      @override 
                      Widget build(BuildContext context,double shrinkOffset,bool overlapsContent){
                        Widget  child = builder(context,shrinkOffset,overlapsContent);

                        return SizedBox.expand(child: child);
                      }
                      // @override
                      // Widget build(
                      //   BuildContext context,
                      //   double shrinkOffset,
                      //   bool overlapsContent,
                      // ) {
                      //   Widget child = builder(context, shrinkOffset, overlapsContent);
                      //   //测试代码：如果在调试模式，且子组件设置了key，则打印日志
                      //   assert(() {
                      //     if (child.key != null) {
                      //       print('${child.key}: shrink: $shrinkOffset，overlaps:$overlapsContent');
                      //     }
                      //     return true;
                      //   }());
                      //   // 让 header 尽可能充满限制的空间；宽度为 Viewport 宽度，
                      //   // 高度随着用户滑动在[minHeight,maxHeight]之间变化。
                      //   return SizedBox.expand(child: child);
                      // }


                      @override
                      double get maxExtent => maxHeight;

                      @override
                      double get minExtent => minHeight;

                      @override
                      bool shouldRebuild(SliverHeaderDelegate old) {
                        return old.maxExtent != maxExtent || old.minExtent != minExtent;
                      }
                    }

                    //示例:
                   /*  class PersistentHeaderRoute extends StatelessWidget {
                      @override
                      Widget build(BuildContext context){
                        
                        return CustomScrollView(
                          slivers: [
                            Text('222'),
                            SliverPersistentHeader(
                              pinned: true,//这里报错暂时先跳过,后期有空再看
                              delegate: SliverHeaderDelegate(//有最大和最小高度
                                maxHeight: 80,
                                minHeight: 50,
                                child: buildHeader(1),
                              ),
                            ),
                          ],
                        );
                      }

                      //头部子元素
                      Widget buildHeader([int num=1]){
                        return Container(
                          color: Colors.red,
                          child: Text('header $num'),
                        );
                      }
                    } */

        //6.11 自定义Sliver:本节将通过自定义2个Sliver,来说明Sliver布局协议,和自定义的过程

          //6.11.1布局协议:
            //1.Viewport将当前布局和配置信息通过SliverConstrains传递给Sliver
            //2.Sliver确认自身位置,绘制等信息存储在geometry中
            //3.viewport读取geometry中信息对Sliver进行布局,绘制

            //SliverConstrains定义:
              /*  class SliverConstraints extends Constraints{

                  //主轴方向
                  AxisDirection axisDirection;
                  //Sliver沿着主轴从列表的哪个方向插入 枚举类型 正向或者反向
                  GrowthDirection growthDirection;
                  //用户滑动方向
                  ScrollDirection userScrollDirection;
                  //当前Sliver划出顶部的总偏移量(有可能是固定在顶部)
                  double scrollOffset;
                  ////当前Sliver之前的Sliver占据的总高度，因为列表是懒加载，如果不能预估时，该值为double.infinity
                  //上述作者表达不清楚(我的理解将要加载的sliver占据的总高度)
                  double precedingScrollExtent;
                    //上一个 sliver 覆盖当前 sliver 的大小，通常在 sliver 是 pinned/floating
                    //或者处于列表头尾时有效，我们在后面的小节中会有相关的例子。(这个也不理解)
                    double overlap;
                    //当前Sliver在Viewport中最大可绘制的区域
                    //如果绘制超过该区域会比较低效(因为不会显示)
                    double remainingPaintExtent;
                    //纵轴的长度,如果滚动是垂直方向,则表示宽度
                    double crossAxisExtent;
                    //纵轴方向
                    AxisDirection crossAxisDirection;
                    //viewport在主轴方向上的高度,若主轴是垂直方向,则表示列表高度
                    double viewportMainAxisExtent;
                    //viewport渲染区域的起点[-Viewport.catchExtent,0]
                    double catchOrigin;
                    //Viewport加载区域的长度
                    //[ViewportMainAxisExtent,viewportMainAxisExtent+Viewport.catchExtent*2]
                    double remainingCatcheExtent;
                } */

              //sliver计算出自身位置,绘制信息然后存储在SliverGeometry中

                // const SliverGeometry({
                //   //Sliver在主轴方向预估长度,大多数情况是固定值,用于计算SliverConstraints.scrollOffset
                //   this.scrollExtent=0.0,
                //   //可视区域绘制长度
                //   this.paintExtent=0.0,
                //   //绘制的坐标原点,相对于自身绘制位置
                //   this.paintOrigin = 0.0,
                //   //在viewport中的长度,如果主轴是垂直方向则表示高度
                //   //范围[0,paintExtent]
                //   double layoutExtent,
                //   //最大绘制长度
                //   this.maxPaintExtent=0.0,
                //   this.maxScrollObstructionExtent=0.0,
                //   //点击测试的范围
                //   double hitTestExtent,
                //   //是否显示
                //   bool visible,
                //   //是否会溢出viewport,如果为true,viewport会剪切
                //   this.hasVisuaOverflow=false,
                //   //scrollExtent修正值,layoutExtent变化后,防止Sliver跳动(应用新的layoutExtent)
                //   //可以先进行修正,具体的作用后面SliverFlexibleHeader再介绍
                //   this.scrollOffsetCorrection,
                //   //预渲染区所占的高度
                //   double catchExtent
                // })

              //Sliver布局和盒布局模型
                //两者布局流程大致相同:父组件将约束信息告诉子组件,子组件根据约束信息确定自身大小,父组件获取子组件绘制信息(大小,位置)
                //不同的是
                  //1.传递约束信息的载体不同,盒模型是BoxConstraints,而Sliver是SliverConstraints
                  //2.子组件布局信息的描述对象不同,盒模型通过size和offset描述,而Sliver通过Geometry描述
                  //3.布局起点不同:Sliver布局起点是viewport,而盒模型可以是任意组件

          //6.11.2 自定义 Sliver(一) SliverFlexibleHeader

            //1.SliverFlexibleHeader

              




                    

            



          



        
          
          






  
              

      

    
          
            


            











  


    

  
                      




    
    

    

  
