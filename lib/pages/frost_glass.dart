import 'package:flutter/material.dart';
import 'dart:ui';//引入ui库 因为ImageFilter widget在这里

class FrostGlass extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('毛玻璃效果'),
      ),
      body: Stack(
        alignment: const FractionalOffset(0.5,0.5),
        children: [
          ConstrainedBox(
            constraints:const BoxConstraints.expand(),
            // child: Image.network('https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.taopic.com%2Fuploads%2Fallimg%2F110603%2F52-11060319503629.jpg&refer=http%3A%2F%2Fimg.taopic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1650508358&t=a71dc16daebc2c8f42db5f0b08f2bf25'),
            child:Image.asset('image/back.png')
          ),
          Center(
          child: ClipRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 5.0,sigmaY: 5.0),
              child: Opacity(
                opacity: 0.1,
                child: Container(
                  width:500.0,
                  height: 700.0,
                  decoration: BoxDecoration(
                    color:Colors.grey.shade200
                  ),
                  child: Center(
                    child: Text(
                      'chen',
                      style: Theme.of(context).textTheme.headline1
                    ),
                  )
                ),
              ),
            ),
          ),
          )
        ],
      ),
    );
  }
}

