import 'package:flutter/material.dart';
class ExpansionTitleDemo extends StatelessWidget{
  List<int> mylist;
  ExpansionTitleDemo(){
    mylist=List<int>();
    for(int i=0;i<10;i++){
      mylist.add(i);
    }
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('闭合实例'),
      ),
      body: Center(
       /*  child: ExpansionTile(
          title: Text('展开'),
          leading:Icon(Icons.ac_unit),
          backgroundColor: Colors.white12,
          children: [
            ListTile(
              title: Text('detail:'),
              leading: Icon(Icons.details),
              subtitle: Text('this is an long story'),
              onTap: (){
                print('delegate');
              },
            ),
            ListTile(
              title: Text('detail:'),
              leading: Icon(Icons.details),
              subtitle: Text('this is an long story'),
              onTap: (){
                print('delegate');
              },
            ),
          ],
        ), */
        child: SingleChildScrollView(//相对于下面那种展开的子元素两旁没有间距
          child: Column(
            children: mylist.map((item){
              return ExpansionTile(
                title: Text('NO.$item'),
                leading: Icon(Icons.house_rounded),
                // backgroundColor: Colors.black26,
                onExpansionChanged: (val){
                  print('val:$val');
                },
                children: [
                  Divider(color: Colors.black38,),
                  ListTile(
                    title: Text('NO.$item'),
                    subtitle: Text('this is a story about $item'),
                    leading: Icon(Icons.find_in_page),
                  ),
                  Divider(color: Colors.black38,),
                  ListTile(
                    title: Text('NO.$item'),
                    subtitle: Text('this is a story about $item'),
                    leading: Icon(Icons.find_in_page),
                  ),
                ],
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}

//闭合列表
 class ExpansionelDemoList extends StatefulWidget {
   _ExpansionelDemoList createState()=> _ExpansionelDemoList();
 }
 class _ExpansionelDemoList extends State<ExpansionelDemoList>{
   //存储控制面板列表的数据
   List<ExpansionControlMap> controlList;
   //构造函数初始化
   _ExpansionelDemoList(){
     //给controlList一个初始默认值
     controlList = List<ExpansionControlMap>();//切结一定要先初始化,否则下面给默认值会报错
     for(int i=0;i<10;i++){
       controlList.add(ExpansionControlMap(i,false));
     }
   }
   //点击当前控制面板交互
   void _cuurentPanelAction(int index,bool bol){
     controlList.forEach((item){
       if(index==item.index){
         item.isOpen = !bol;
       }
     });
   }
   @override
   Widget build(BuildContext context){
     return Scaffold(
       appBar: AppBar(
         title: Text('闭合列表'),
       ),
       body: Center(
         child:SingleChildScrollView(//切记ExpansionPaneList外面要套singleChildScrollView
          child: ExpansionPanelList(
            expansionCallback:(index,bol){
              setState(() {
                _cuurentPanelAction(index,bol);
              });
            },
            children: controlList.map((item){
              return ExpansionPanel(
                headerBuilder:(context,isopened){
                  return ListTile(title: Text('NO.${item.index}'),);
                },
                body: ListTile(
                  title: Text('No.${item.index}'),
                  subtitle: Text('this is story about ${item.index}'),
                  leading: Icon(Icons.pan_tool),
                ),
                isExpanded: item.isOpen
              );
            }).toList(),
          ),
         )
         
       ),
     );
   }
 }
 //控制面板的map
 class ExpansionControlMap{
   int index;
   bool isOpen;
   ExpansionControlMap(this.index,this.isOpen);
 }