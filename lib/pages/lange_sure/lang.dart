//底部导航效果
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomNavigatorPage extends StatefulWidget {
  _BottomNavigatorPage createState()=>_BottomNavigatorPage();
}
class _BottomNavigatorPage extends State<BottomNavigatorPage>{
  List<Widget> _widgetList = List<Widget>();
  int _myindex=0;
  @override
  initState(){
    _widgetList.add(BodyWidget('新闻内容'));
    _widgetList.add(BodyWidget('历史内容'));
    _widgetList.add(BodyWidget('经济内容'));
    super.initState();
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('底部导航栏'),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            title: Text('新闻'),
            icon: Icon(Icons.message,color: Colors.green,),
          ),
          BottomNavigationBarItem(
            title: Text('历史'),
            icon: Icon(Icons.history,color: Colors.green,),
          ),
          BottomNavigationBarItem(
            title: Text('经济'),
            icon: Icon(Icons.money,color: Colors.green),
          ),
        ],
        selectedItemColor: Colors.yellow,
        selectedFontSize: 20.0,
        selectedLabelStyle: TextStyle(
          color: Colors.yellow
        ),
        currentIndex: _myindex,
        type:BottomNavigationBarType.fixed,
        onTap: (index){
          print('tap:$index');
          print('aaa');
          setState(() {
            _myindex = index;
          });
        },

        
      ),
      body: _widgetList[_myindex],
    );
  }
}
//WidgetContaier
class BodyWidget extends StatelessWidget {
  final String mytitle;
  BodyWidget(this.mytitle);
  @override
  Widget build(BuildContext context){
    return Center(
      child: Container(
        color: Colors.blueAccent,
        child: Text(mytitle),
      ),
    );
  }
}

//不规则底部导航栏
class NoRuleBottomNavigatorDemo extends StatelessWidget {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        title: Text('不规则导航栏'),
      ),
      floatingActionButton:FloatingActionButton(
        onPressed: (){
          print('++1');
        },
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar:BottomAppBar(
        color: Colors.lightBlue,
        shape: CircularNotchedRectangle(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.max,
          children: [
            IconButton(
              icon: Icon(Icons.home),
              onPressed: (){
                print('home');
              },
               color:Colors.white
            ),
            IconButton(
               color:Colors.white,
              icon: Icon(Icons.car_repair),
              onPressed: (){
                print('car');
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: Text('内容'),
      ),
    );
  }
}

//跳转页面翻转
class JumpRoutePageAnimation extends StatelessWidget {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('routePageJumpA'),
      ),
      body: Center(
        child: ElevatedButton.icon(
          icon: Icon(Icons.search),
          label: Text('跳转到查询页面'),
          onPressed: (){
            // Navigator.of(context).push(
            //   MaterialPageRoute(
            //     builder:(context){
            //       // return SecondPage();
            //       return CustomAnimation(SecondPage());
            //     }
            //   )
            // );
            Navigator.of(context).push(CustomAnimation(SecondPage()));
          },
        ),
      ),
      drawer: Drawer(
        child: Text('cc'),
      ),
    );
  }
}
class SecondPage extends  StatelessWidget{
  @override
  Widget build(BuildContext con){
    return Scaffold(
      appBar: AppBar(
        title: Text('seconds'),
      ),
      body: Center(
        child: Text('secondsPage'),
      ),
    );
  }
}
//路由跳转动画
class CustomAnimation extends PageRouteBuilder{
  Widget mywidget;
  CustomAnimation(this.mywidget):super(
    transitionDuration: const Duration(seconds: 1),
    pageBuilder:(BuildContext context,Animation<double> animation1,Animation<double> animation){
      return mywidget;
    },
    transitionsBuilder:(BuildContext context,Animation<double> animation1,Animation<double> animation2,Widget child){
      return FadeTransition(
        opacity: Tween(begin: 0.0,end: 1.0).animate(
          CurvedAnimation(
            curve: Curves.fastOutSlowIn,
            parent: animation1
          ),
        ),
        child: RotationTransition(
          turns: Tween(begin: 0.0,end:1.0).animate(
            CurvedAnimation(
              curve: Curves.fastOutSlowIn,
              parent: animation1
            )
          ),
          child: ScaleTransition(
              scale: Tween(begin: 2.0,end: 1.0).animate(
                CurvedAnimation(
                  curve: Curves.fastOutSlowIn,
                  parent: animation1
                )
              ),
              child: child,
              
              
            ),
        ),
      );
    }
  );
}

//毛玻璃效果
class FrostGlassDemo extends StatelessWidget{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('毛玻璃效果'),
      ),
      body: Center(
        child: Stack(
          children: [
            ConstrainedBox(
              constraints: const BoxConstraints.expand(),
              child: Image.asset('image/back.png',fit: BoxFit.fill,),
            ),
            ClipRect(
              child: BackdropFilter(
                filter:ImageFilter.blur(sigmaX: 5.0,sigmaY: 5.0),
                child: Opacity(
                  opacity: 0.2,
                  child: Container(
                    width: 500.0,
                    height: 500.0,
                    color: Colors.grey.shade100,
                    decoration: BoxDecoration(
                      
                    ),
                    child: Text('哈哈哈哈',style: TextStyle(color: Colors.black12),),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
//保持状态测试一
class KeepStateDemo extends StatefulWidget {
  _KeepStateDemo createState()=> _KeepStateDemo();
}
class _KeepStateDemo extends State<KeepStateDemo> with SingleTickerProviderStateMixin{
  TabController _controller;
  int mynum=0;
  @override
  void initState(){
    _controller= TabController(length: 3,vsync: this);
    super.initState();
  }
  @override
  void dispose(){
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('保持页面状态'),
        bottom: TabBar(
          controller: _controller,
          tabs: [
            Tab(
              text: '政治',
              icon: Icon(Icons.face),
            ),
            Tab(
              text: '经济',
              icon: Icon(Icons.face),
            ),
            Tab(
              text: '文化',
              icon: Icon(Icons.face),
            ),
          ],
        ),
      ),
      floatingActionButton:FloatingActionButton(
        onPressed: (){
          setState(() {
            mynum++;
          });
        },
        tooltip: '加加',
        child: Icon(Icons.add),
      ),
      body: Center(
        child: TabBarView(
          controller: _controller,
          children: [
            Text('demo1:$mynum'),
            Text('demo2:$mynum'),
            RaisedButton(
              onPressed: (){
                Navigator.of(context).push(CustomAnimation(SecondPage()));
              },
              child: Text('demo2:$mynum跳转'),
              
            )
          ],
        ),
      ),
    );
  }
}

//搜索栏

class SeachBarDemo extends StatefulWidget {
  _SeachBarDemo createState()=> _SeachBarDemo();
}
class _SeachBarDemo extends State<SeachBarDemo> {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('搜索栏导航'),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){
              showSearch(context: context, delegate: SearchPageDemo());
            },
          )
        ],
      ),
      body: Center(
        child: Text('搜素栏导航')
      ),
    );
  }
}
//搜索页面
class SearchPageDemo extends SearchDelegate<String> {
  @override
  List<Widget> buildActions(BuildContext context){
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: (){
          query='';
        },
      )
    ];
  }
  @override
  Widget buildLeading(BuildContext context){
    return IconButton(
      icon: AnimatedIcon(icon: AnimatedIcons.menu_arrow,progress: transitionAnimation),
      onPressed: (){
        close(context,null);
      },
    );
  }
  @override
  Widget buildResults(BuildContext context){
    return Container(
      child: Center(
        child: Text(query),
      ),
    );
  }
  @override
  Widget buildSuggestions(BuildContext context){
    return Text('aa');
  }
}

//选择图片
class SelectPhotoDemo extends StatefulWidget {
  _SelectPhotoDemo createState()=> _SelectPhotoDemo();
}
class _SelectPhotoDemo extends State<SelectPhotoDemo>{
  List<Widget> mylist;
  @override
  void initState(){
    mylist = List<Widget>();
    mylist.add(buildAddButton());
    super.initState();
  }
  @override
  Widget build(BuildContext context){
    final width = MediaQuery.of(context).size.width;
    final heigth = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar:AppBar(
        title: Text('选择图片'),
      ),
      body: Container(
        width: width,
        height: heigth /2,
        // child: Text('选择图片'),
        child: Wrap(
          children: mylist,
          spacing: 20.0,
        ),
      ),
    );
  }
  //增加图片按钮
  Widget buildAddButton(){
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Container(
            width: 80.0,
            height: 80.0,
            color: Colors.red,
            child: Center(
              child: Icon(Icons.add),
            ),
        ),
      ),
      onTap: (){
        setState(() {
          mylist.insert(mylist.length -1,buildContainerDemo());
        });
      },
    );
  }
  //模拟图片容器
  Widget buildContainerDemo(){
    // return Container(
    //   padding: const EdgeInsets.all(20.0),
    //   color:Colors.green,
    //   width: 80.0,
    //   height: 80.0,
    //   child: Center(
    //     child: Text('图片'),
    //   ),
    // );
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child:  Container(
        color:Colors.green,
        width: 80.0,
        height: 80.0,
        child: Center(
          child: Text('图片'),
        ),
      )
    );
  }
}

//展开闭合实例
class ExpandedCloseDemo extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: Text('展开闭合实例'),),
      body: Center(
        child: ExpansionTile(
          title: Text('政治'),
          children: [
            Text('啊啊啊'),
            RichText(
              text:TextSpan(
                text: '1111',
                children: [
                  TextSpan(
                    text:'2222'
                  )
                ],
                style: TextStyle(
                  color: Colors.red
                )
              ),
            )
          ],
          initiallyExpanded: false,
        ),
      ),
    );
  }
}
//展开闭合列表
class ExpandedCloseList extends StatefulWidget{
  _ExpandedCloseList createState()=> _ExpandedCloseList();
}
class _ExpandedCloseList extends State<ExpandedCloseList>{
  List<ExpandedListMap> mylist;
  List<int> numList = List<int>();
  @override
  void initState(){
    mylist = List<ExpandedListMap>();
    for(var myindex=0;myindex<10;myindex++){
      mylist.add(
        ExpandedListMap(myindex,false)
      );
      numList.add(myindex);
    }
    
    super.initState();
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('闭合列表'),
        
      ),
      body: Center(
        /* child: ExpansionPanelList(
          expansionCallback:(index,bol){
            setState(() {
              mylist = mylist.map((item){
                if(item.index==index){
                  item.flag = !bol;
                }
                return item;
              }).toList();
            });
          },
          children: numList.map((myindex){
            return ExpansionPanel(
              headerBuilder: (context,index){
                return ListTile(
                  title: Text('标题$myindex'),
                );
              },
              body: ListTile(
                title: Text('内容$myindex'),
              ),
              isExpanded: mylist[myindex].flag
            );
          }).toList(),
          
        ), */
        child: SingleChildScrollView(//一定外面记得加这个组件否则会报错
          child: ExpansionPanelList(
            expansionCallback:(index,bol){
              setState(() {
                mylist = mylist.map((item){
                  if(item.index==index){
                    item.flag = !bol;
                  }
                  return item;
                }).toList();
              });
            },
            children: numList.map((myindex){
              return ExpansionPanel(
                headerBuilder: (context,index){
                  return ListTile(
                    title: Text('标题$myindex'),
                  );
                },
                body: ListTile(
                  title: Text('内容$myindex'),
                ),
                isExpanded: mylist[myindex].flag
              );
            }).toList(),
            
          ),  
        ),

      ),
    );
  }
}
//闭合列表数据
class ExpandedListMap {
  //下标
  int index;
  //闭合flag
  bool flag;
  //
  ExpandedListMap(this.index,this.flag);
}

//贝塞尔曲线
class BeiSaiErDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        title: Text('贝塞尔曲线demo')
      ),
      body: Container(
        height: 300.0,
        color: Colors.green,
        child: ClipPath(
          clipper: CustomClipperDemo(),
          child: Container(
            height: 200.0,
            color:Colors.blue[700]
          ),
        ),
      ),
    );
  }
}
class CustomClipperDemo extends CustomClipper<Path>{
  @override
  Path getClip(Size size){
    var path = Path();
    path.lineTo(0.0,0.0);
    path.lineTo(0, size.height - 30);
    var firstPointstart = Offset(size.width/2,size.height);
    var firstPointEnd = Offset(size.width,size.height - 30);
    path.quadraticBezierTo(firstPointstart.dx,firstPointstart.dy,firstPointEnd.dx,firstPointEnd.dy);
    path.lineTo(size.width, size.height - 30);
    path.lineTo(size.width,0.0);

    return path;
  }
  @override
  bool shouldReclip(CustomClipper<Path> oldClipper){
    return false;
  }
}

class OpenPageAnimation extends StatefulWidget {
  @override
  _OpenPageAnimation createState()=>_OpenPageAnimation();
}
class _OpenPageAnimation extends State<OpenPageAnimation> with SingleTickerProviderStateMixin{
  AnimationController _animationController;
  Animation _animation;
  @override
  initState(){
    _animationController= AnimationController(vsync:this,duration:Duration(milliseconds: 3000));
    _animation = Tween(begin: 0.0,end:1.0).animate(_animationController);
    _animation.addStatusListener((status){
      if(status==AnimationStatus.completed){
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context)=>MyTestPage()), 
          (route)=> route==null);
      }
    });
    super.initState();
    //播放动画
    _animationController.forward();
  }
  @override
  void dispose(){
    _animationController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context){
    return FadeTransition(opacity: _animationController,child: Image.asset('image/back.png',fit: BoxFit.cover,scale: 2.0,));
  }
}
class MyTestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('测试页'),
      ),
    );
  }
}
//ios-ui

class IosBackDemo extends StatelessWidget {
  
  @override
  Widget build(BuildContext context){
    return CupertinoPageScaffold(
      child: Center(
        child: CupertinoButton(
          child: Text('跳转'),
          onPressed: (){
            Navigator.of(context).push(
              CupertinoPageRoute(
                builder: (BuildContext context){
                  return IosSecondsPage();
                }
              )
            );
          },
        ),
      ),
    );
  }
}
class IosSecondsPage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return CupertinoPageScaffold(
      child: Center(
        child: CupertinoButton(
          child: Text('返回'),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
      ),
    );
  }
} 

//tooltip组件
class MyTolltipDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(title:Text('tooptip组件')),
      body:Center(
        child: Container(
          width:300.0,
          height: 300.0,
          color:Colors.blue,
          child: Tooltip(
            message: '哈哈哈哈',
            child: Icon(Icons.add),
          ),
        ),
      )
    );
  }
}

//拖拽组件
// class MyDraggaleDemo extends StatefulWidget{
//   @override
//   _MyDraggaleDemo createState()=> _MyDraggaleDemo();
// }
class MyDraggaleDemo extends StatelessWidget {
  // final Color _draggableColor = Colors.grey;
  @override
  Widget build(BuildContext context){
     Color _draggableColor = Colors.grey;
    return Scaffold(
      appBar:AppBar(
        title: Text('拖拽组件')
      ),
      body: Center(
        child: Stack(
          children: [
            DraggleMyDemo(offset: Offset(80.0,80.0),widgetColor: Colors.blue),
            DraggleMyDemo(offset: Offset(160.0,80.0),widgetColor: Colors.red),
            Center(//特别注意,这里一定要注意dratarge的位置,如果不给位置,不加center会覆盖前面元素
              child:  Container(
                child: DragTarget(
                  onAccept:(Color color){
                    _draggableColor= color;
                  },
                  builder: (context,candidateData,rejectedData){
                    return Container(
                      width: 200.0,
                      height: 200.0,
                      color: _draggableColor,
                    );
                  },
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}

//拖拽内容
class DraggleMyDemo extends StatefulWidget {
  final Offset  offset;
  final Color widgetColor;
  //常量类
  const DraggleMyDemo({Key key,this.offset,this.widgetColor}):super(key:key);
  @override
  _DraggleMyDemo createState()=> _DraggleMyDemo();
}
class _DraggleMyDemo extends State<DraggleMyDemo>{
  Offset offset = Offset(0.0, 0.0);
  @override
  void initState(){
     super.initState();
    offset = widget.offset;
    print('offset:$offset----${widget.widgetColor}');
    // super.initState();
  }
  @override
  Widget build(BuildContext context){
   return Positioned(
    left:offset.dx,
    top:offset.dy,
    child: Draggable(
      data: widget.widgetColor,
      child: Container(
        width: 80.0,
        height: 80.0,
        color: widget.widgetColor,
      ),
      feedback: Container(
        width: 80.0,
        height: 80.0,
        color: widget.widgetColor.withOpacity(0.5),
      ),
      onDraggableCanceled: (Velocity velocity,Offset offset){
        setState(() {
          // this.offset = offset;
        });
      },
    ),
   );
  }
}