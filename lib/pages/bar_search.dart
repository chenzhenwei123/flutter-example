import 'dart:convert';

import 'package:flutter/material.dart';

const List infoList = ['huawei phone','xiaomi phone','apple phone','sanxing phone'];
class BarSearchDemo extends StatefulWidget {
  _BarSearchDemo createState() => _BarSearchDemo();
}
class _BarSearchDemo extends State<BarSearchDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: Text('搜索demo'),
        actions: [
          IconButton(
            icon: Icon(Icons.search,color: Colors.purple,),
            color: Colors.purple,
            onPressed: (){
              print('211');
              showSearch(context: context, delegate: SearchInputDemo());
            },
          )
        ],
      )
    );
  }
}

//搜索输入
class SearchInputDemo extends SearchDelegate<String> {
  //重写右边清除功能
  @override
  List<Widget> buildActions(BuildContext context){
    return [IconButton(icon: Icon(Icons.clear), onPressed: (){
      return query='';
    })];
  }
  //重写左边关闭功能
  @override
  Widget buildLeading(BuildContext context){
    return IconButton(icon: AnimatedIcon(icon: AnimatedIcons.menu_arrow,progress: transitionAnimation,), onPressed: (){
      close(context,null);
    });
  }
  //重写搜索结果展示方法
  @override
  Widget buildResults(BuildContext context){
    return Container(
      child: Card(
        color: Colors.redAccent,
        child: Text(query,
          style: TextStyle(
            color: Colors.green,
            fontSize: 20.0
          ),
        ),
      ),
      width:500.0,
      height: 700.0,
    );
  }
  //重写建议功能
  @override
  Widget buildSuggestions(BuildContext context){
    List suggestionList= query.isEmpty?json.decode(json.encode(infoList)):infoList.where((item){
      return item.startsWith(query);
    }).toList();
    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder:(context,index){
        return ListTile(
          title: RichText(
            text:TextSpan(
              // text:suggestionList[index].substring(0,query.length),
              // style: TextStyle(
              //   color:Colors.black,
              //   fontWeight: FontWeight.bold
              // ),
              children: [
                TextSpan(
                  text: suggestionList[index].substring(0,query.length),
                  style:TextStyle(
                    color:Colors.black,
                    fontWeight: FontWeight.bold
                  )
                ),
                TextSpan(
                  text: suggestionList[index].substring(query.length),
                  style:TextStyle(
                    color: Colors.grey
                  )
                ),
              ]
              
            ),
          ),
        );
      }
    );
  }

}