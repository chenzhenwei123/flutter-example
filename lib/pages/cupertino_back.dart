import 'package:flutter/cupertino.dart';
class CupertinoBackDemo extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return CupertinoPageScaffold(
      child: Center(
        child: Container(
          width:100.0,
          height: 100.0,
          color: CupertinoColors.activeBlue,
          child: CupertinoButton(
            child: Icon(CupertinoIcons.cursor_rays,color:CupertinoColors.activeGreen),
            onPressed: (){
              Navigator.of(context).push(
                CupertinoPageRoute(
                  builder: (context){
                    return CupertinoTargetDemo();
                  }
                )
              );
            },
          ),
        ),
      ),
    );
  }
}

class CupertinoTargetDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return CupertinoPageScaffold(
      child: Center(
        child: Container(
          width: 200.0,
          height: 300.0,
          child: CupertinoButton(
            child: Icon(CupertinoIcons.back),
            onPressed: (){
              Navigator.of(context).push(CupertinoPageRoute(
                builder: (context){
                  return CupertinoBackDemo();
                }
              ));
            },
          ),
          color:CupertinoColors.systemGreen
        ),
      ),
    );
  }
}