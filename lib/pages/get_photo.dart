import 'package:flutter/material.dart';

class GetPhotoDemo extends StatefulWidget {
  _GetPhotoDemo createState()=>_GetPhotoDemo();
}
class _GetPhotoDemo extends State<GetPhotoDemo> {
  List<Widget> mylist=List<Widget>();
  @override
  void initState(){
    mylist.add(
      _addPhotoButton()
    );
    super.initState();
  }
  @override
  Widget build(BuildContext context){
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text('添加照片'),
      ),
      body: Center(
        child: Container(
              width: width,
              height: height/2,
              color: Colors.black87,
              child: Opacity(
                opacity: 0.6,
                child: Wrap(
                  children: mylist,
                  spacing: 26.0,
                ),
              ),
            ),
      )
    );
  }
  //添加照片组件
  Widget _addPhotoButton(){
    return GestureDetector(
      onTap: (){
        setState(() {
          if(mylist.length<9){
            mylist.insert(mylist.length-1, _getPhotoWidget());
          }
          
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: 80.0,
          height: 80.0,
          color: Colors.amber,
          child: Center(
            child: Icon(Icons.add,color:Colors.black54,size: 20.0,),
          ),
        ),
      ),
    );
  }
  //生成组件图片
  Widget _getPhotoWidget(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 80.0,
        height: 80.0,
        color: Colors.redAccent,
        // child:Image.asset('image/back.png')
        child: Text('照片',style: TextStyle(color: Colors.white),),
      ),
    );
  }
}

//添加照片组件
