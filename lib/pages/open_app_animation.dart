import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import './besier_line_demo.dart';

class OpenAppAnimation extends StatefulWidget{
  _OpenAppAnimation createState()=> _OpenAppAnimation();
}
class _OpenAppAnimation extends State<OpenAppAnimation> with SingleTickerProviderStateMixin {
  AnimationController _controll;
  Animation _animation;
  @override
  void initState(){
    _controll=AnimationController(vsync: this,duration: Duration(microseconds:3000));
    _animation = Tween(begin: 0.0,end: 1.0).animate(_controll);
    super.initState();
    _animation.addStatusListener((status) {
      if(status==AnimationStatus.completed){
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>BezierToDemoTest()), (route) => route==null);
      }
    });
    _controll.forward();//执行动画
  }
  @override
  void dispose(){
    super.dispose();
    _controll.dispose();
  }
  @override
  Widget build(BuildContext context){
    return FadeTransition(
      opacity: _animation,
      child: Image.network(
        'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.taopic.com%2Fuploads%2Fallimg%2F110603%2F52-11060319503629.jpg&refer=http%3A%2F%2Fimg.taopic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1650508358&t=a71dc16daebc2c8f42db5f0b08f2bf25',
        fit: BoxFit.cover,
        scale: 2.0,
      ),
    );
  }
}