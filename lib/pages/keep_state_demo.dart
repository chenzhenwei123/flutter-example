import 'package:flutter/material.dart';

class KeepPageStateDemo extends StatefulWidget {
  _KeepPageStateDemo createState()=> _KeepPageStateDemo();
}
class _KeepPageStateDemo extends State<KeepPageStateDemo> with SingleTickerProviderStateMixin{//混入相关方法
  TabController _controller;//控制器
  int _count=0;
  @override
  void initState(){
    //初始化控制器
    _controller = TabController(length: 3,vsync: this);
    super.initState();
  }
  //重写dispose方法 只释放控制器
  @override
  void dispose(){
    _controller.dispose();
    super.dispose();
  }
  void _addCount(){
    setState(() {
      _count++;
    });
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('保持页面状态'),
      // ),
      floatingActionButton: FloatingActionButton(
        tooltip: '++',
        child: Icon(Icons.add,size: 20.0,color: Colors.white),
        onPressed: _addCount,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue,
        child: TabBar(
          controller: _controller,
          tabs: [
            Tab(
              icon: Icon(Icons.car_rental),
              child: Text('车'),
            ),
            Tab(
              icon: Icon(Icons.house),
              child: Text('房'),
            ),
            Tab(
              icon: Icon(Icons.palette),
              child: Text('plane'),
            )
          ],
          onTap: (int index){
            print('index:$index');
          },
        ),
      ),
      appBar: AppBar(
        title: Text('保持页面状态'),
      ),
      body:TabBarView(
          controller: _controller,
          children: [
            Text('页面1number:$_count',style:TextStyle(
              color: Colors.black
            )),
            Text('页面2number:$_count',style:TextStyle(
              color: Colors.black
            )),
            Text('页面3number:$_count',style:TextStyle(
              color: Colors.black
            ))
          ],
        ),
      
    );
  }

}

//不使用控制器的简便写法,但是这种页面不能修改值
 class SimpleMethods extends StatelessWidget {
   @override
   Widget build(BuildContext context){
     List _titleList= ['新闻','历史','政治'];
     return DefaultTabController(
       length: _titleList.length,
       child: Scaffold(
         appBar: AppBar(
           title: Text('简易'),
           bottom: TabBar(
            tabs: _titleList.map((item){
              return Tab(
                child: Text(item),
              );
            }).toList()
          ),
         ),
         body: TabBarView(
           children: _titleList.map((item){
             return Center(
               child: Text(item),
             );
           }).toList(),
         ),
       ),
     );
   }
 }