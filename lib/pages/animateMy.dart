import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//渐现+滑动
class CustomAnimat extends PageRouteBuilder {
  final Widget mywidget;
  CustomAnimat(this.mywidget):super(
    transitionDuration: const Duration(seconds: 4),
    pageBuilder:(BuildContext context,Animation<double> animation1,Animation<double> animation2){
      return mywidget;
    },
    transitionsBuilder:(BuildContext context,Animation<double> animation1,Animation<double> animation2,Widget mywidget){
      return FadeTransition(
        opacity:Tween(begin: 0.0,end: 1.0).animate(CurvedAnimation(
          curve:Curves.fastOutSlowIn,
          parent:animation1
        )),
        child: SlideTransition(
          position: Tween(begin: Offset(-1.0,0.0),end: Offset(0.0,0.0)).animate(
            CurvedAnimation(
              parent: animation1,
              curve:Curves.fastOutSlowIn
            )
          ),
          child: mywidget,
        ),
      );
    }
  );
}
//渐现+旋转+缩放
class CustorAnimation2 extends PageRouteBuilder {
  final Widget mywidget;
  CustorAnimation2(this.mywidget):super(
    transitionDuration: const Duration(seconds: 3),
    pageBuilder:(BuildContext context,Animation<double> animation1,Animation<double> animation2){
      return mywidget;
    },
    transitionsBuilder:(BuildContext context,Animation<double> animation1,Animation<double> animation2,Widget child){
      return FadeTransition(
        opacity:Tween(begin: 0.0,end: 1.0).animate(
          CurvedAnimation(
            parent: animation1,
            curve: Curves.fastOutSlowIn
          )
        ),
        child: ScaleTransition(
          scale: Tween(begin: 0.5,end: 1.0).animate(
            CurvedAnimation(
              parent: animation1,
              curve: Curves.fastOutSlowIn
            )
          ),
          child:RotationTransition(
            turns: Tween(begin: 0.2,end: 2.0).animate(
              CurvedAnimation(
                parent: animation1,
                curve:Curves.fastOutSlowIn
              )
            ),
            child: child,
          ),
        ),
      );
    }
  );
}
class MyPage1 extends StatelessWidget {
  // List<Widget> _pageList = List<Widget>();
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor:Colors.blue,
      appBar:AppBar(
        title: Text(
          '首页',
          style: TextStyle(
            color:Colors.black38,
            fontSize: 30.0,
            decoration: TextDecoration.lineThrough,
            decorationStyle: TextDecorationStyle.solid
          ),
        ),
      ),
      body:Center(
        child: MaterialButton(
          onPressed: (){
            // _navigatorTo(context);
            Navigator.of(context).push(CustomAnimat(SecondsPage()));
          },
          child: Icon(
            Icons.next_plan_outlined,
            color: Colors.green,
            size:30.0
          ),
        )
      )
    );
  }
  // _navigatorTo(BuildContext context) async{
  //   var result = await Navigator.push(
  //     context,MaterialPageRoute(
  //       builder:(context){
  //         return CustomAnimat(SecondsPage());//SecondsPage()
  //       }
  //     )
  //   );
  //  当使用同步是动画组件并不能使用
  //   print('result:啊啊啊啊:$result');
  // }
}

class SecondsPage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.pinkAccent,
      appBar:AppBar(
        title: Text(
          '副页',
          style: TextStyle(
            color:Colors.green,
            fontSize: 30.0,
            decoration: TextDecoration.lineThrough,
            decorationStyle: TextDecorationStyle.solid
          ),
        ),
        backgroundColor: Colors.yellowAccent,
      ),
      body: Center(
        child: IconButton(
          icon: Icon(
            Icons.navigate_before,
            size:30.0,
            color: Colors.yellow,
          ),
          onPressed: (){
            // Navigator.pop(context,'成功');
            // Navigator.of(context).pop();
            Navigator.of(context).push(CustorAnimation2(MyPage1()));
            //这里存在一个问题:使用Navigator.of(context).pop()和Navigator.pop(context,'成功')时并没有动画

          },
        ),
      ),
    );
  }
}

