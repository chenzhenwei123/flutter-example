import 'package:flutter/material.dart';

//创建一个动态页面
class ChangePage extends StatefulWidget {
  String _title;
  ChangePage(this._title);
  _ChangePage createState()=>_ChangePage();
}
class _ChangePage extends State<ChangePage>{
  @override
  Widget build(BuildContext context){
    return Center(
      child: Text(widget._title),
    );
  }
}
class BottomBarShape extends StatefulWidget {
  _BottomBarShape createState()=>_BottomBarShape();
}
class _BottomBarShape extends State<BottomBarShape> {
  int _index=0;
  List<Widget> _pageList = List<Widget>();
  @override
  void initState(){
    super.initState();
    _pageList.add(ChangePage('首页$_index'));//这里的_index取的初始化的值,传到页面里不会改变
    _pageList.add(ChangePage('查找$_index'));
  }
  @override
  Widget build(BuildContext cont){
    return Scaffold(
      body: Center(
        child: _pageList[_index],
        // child:Text('啊啊啊啊'),
      ),
      floatingActionButton:FloatingActionButton(
        tooltip: '加加',
        onPressed: (){
          setState(() {
            if(_index>0){
              _index=0;
            }else{
              _index++;
            }
          });
        },
        child: Icon(
          Icons.add,
          color:Colors.white
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue,
        shape:CircularNotchedRectangle(),
        child: Row(
          mainAxisSize:MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              icon: Icon(Icons.home),
              color: Colors.white,
              onPressed: (){
                print('_index:$_index');
                setState(() {
                  _index=0;
                });
              },
            ),
            IconButton(
              icon: Icon(Icons.find_in_page),
              color: Colors.white,
              onPressed: (){
                setState(() {
                  _index=1;
                });
                print('_index:$_index');
                print(_pageList[0]);
              },
            ),
          ],
        ),
      ),
    );
  }
}
