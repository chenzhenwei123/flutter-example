import 'package:flutter/material.dart';

//draggable控件
class DraggableWidget extends StatefulWidget {
  final Offset offset;//记得加上final声明,否则报错
  final Color widgetColor;//记得加上final声明,否则报错
  final String title;
  const DraggableWidget({Key key,this.offset,this.widgetColor,this.title}):super(key: key);
  _DraggableWidget createState()=>_DraggableWidget();
}
class _DraggableWidget extends State<DraggableWidget>{
      Offset offset = Offset(0.0,0.0);
      @override
      void initState(){
        print('mywidget:$widget');
        offset = widget.offset;
        super.initState();
      }
      @override
      Widget build(BuildContext context){
        return Positioned(
          left: offset.dx,
          top:offset.dy,
          child: Draggable(
            data:MyData(widget.title,widget.widgetColor),
            child: Container(
              width: 100.0,
              height: 100.0,
              color: Colors.blue,
              child: Text(widget.title),
            ),
            feedback: Container(
              width: 80.0,
              height: 80.0,
              color: Colors.greenAccent,
            ),
            onDraggableCanceled:(Velocity velocity,Offset offset){
              setState(() {
                //关于this和widget的问题需要弄清楚
                // this.offset = offset;
              });
            }
          ),
        );
      }
}
//data class
class MyData {
  String title;
  Color _draggableColor;
  MyData(this.title,this._draggableColor);
}
//draggable target控件
class DraggableTragetWidget extends StatefulWidget{
  _DraggableTragetWidget createState()=>_DraggableTragetWidget();
}
class _DraggableTragetWidget extends State<DraggableTragetWidget>{
  Color _draggableColor = Colors.grey;
  String mytitle ='';
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('拖拽'),
      ),
      body: Center(
        child: Stack(
          children: [
            DraggableWidget(
              offset: Offset(20.0,80.0),
              widgetColor: Colors.green,
              title: 'box1',
            ),
            DraggableWidget(
              offset: Offset(100.0,80.0),
              widgetColor: Colors.red,
              title: 'box2',
            ),
            Center(//特别注意,这里一定要注意dratarge的位置,如果不给位置,不加center会覆盖前面元素
              child: DragTarget(
                onAccept:(MyData mapData){
                  _draggableColor = mapData._draggableColor;
                  mytitle = mapData.title;
                },
                builder: (context,candidateData,rejectedData){
                  return Container(
                    child: Text(mytitle),
                    width: 100.0,
                    height: 100.0,
                    color: _draggableColor,
                  );
                },
              )
            )
          ],
        )
      ),
    );
  }
}