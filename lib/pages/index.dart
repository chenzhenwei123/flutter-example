import 'package:flutter/material.dart';
/* 简单的切换效果 */
class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Center(
      child: Text('首页内容'),
    );
  }
}
class MyFind extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Center(
      child: Text('发现页'),
    );
  }
}
class MySearch extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Center(
      child: Text('查找页'),
    );
  }
}
class MyMine extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Center(
      child: Text('个人中心'),
    );
  }
}
class BottomNavigationBarClass extends StatefulWidget {
  _BottomNavigationBarClass createState()=> _BottomNavigationBarClass();
}
class _BottomNavigationBarClass extends State<BottomNavigationBarClass>{
  final _fontColor = Colors.blue;//全局颜色
  List<Icon> _icons=[Icon(Icons.home,color:Colors.blue),Icon(Icons.search,color:Colors.blue),Icon(Icons.find_in_page,color:Colors.blue),Icon(Icons.face_sharp,color:Colors.blue)];
  List<String> _titleList = ['首页','发现','查找','我的'];
  int _current=0;
  List<Widget> _pageList=List();
  @override
  void initState(){
    _pageList.add(MyHome());
    _pageList.add(MyFind());
    _pageList.add(MySearch());
    _pageList.add(MyMine());
    super.initState();
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        // leading: _icons[_current],
        title:Center(
          child: Text(_titleList[_current]),
        )
      ),
      body:_pageList[_current],
      bottomNavigationBar:BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            // icon: _icons[0],
            icon:Icon(Icons.home,color:Colors.blue),
            title: Text(
              '首页',
              style:TextStyle(
                color: Colors.blue
              )
            )
            
          ),
          BottomNavigationBarItem(
            icon: _icons[1],
            title: Text(_titleList[1],style:TextStyle(color: _fontColor)),
            
          ),
          BottomNavigationBarItem(
            icon: _icons[2],
            title: Text(_titleList[2],style:TextStyle(color: _fontColor)),
            
          ),
          BottomNavigationBarItem(
            icon: _icons[3],
            title: Text(_titleList[3],style:TextStyle(color: _fontColor)),
            
          ),
        ],
        currentIndex: _current,
        onTap: (int index){
          setState(() {
            _current=index;
          });
        },
        type: BottomNavigationBarType.fixed,
      )
    );
  }
}